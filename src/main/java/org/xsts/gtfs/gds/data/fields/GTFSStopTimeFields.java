/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSStopTimeFields {

    public static final String TRIP_ID = "trip_id";
    public static final String ARRIVAL_TIME = "arrival_time";
    public static final String DEPARTURE_TIME = "departure_time";
    public static final String STOP_ID = "stop_id";
    public static final String STOP_SEQUENCE = "stop_sequence";
    public static final String STOP_HEADSIGN = "stop_headsign";
    public static final String PICKUP_TYPE = "pickup_type";
    public static final String DROP_OFF_TYPE = "drop_off_type";
    public static final String CONTINUOUS_PICKUP = "continuous_pickup";
    public static final String CONTINUOUS_DROP_OFF = "continuous_drop_off";
    public static final String SHAPE_DIST_TRAVELED = "shape_dist_traveled";
    public static final String TIMEPOINT = "timepoint";

    public static final String TRIP = TRIP_ID;
    public static final String ARRIVAL = ARRIVAL_TIME;
    public static final String DEPARTURE = DEPARTURE_TIME;
    public static final String STOP = STOP_ID;
    public static final String SEQUENCE = STOP_SEQUENCE;
    public static final String HEADSIGN = STOP_HEADSIGN;
    public static final String PICKUP = PICKUP_TYPE;
    public static final String DROP_OFF = DROP_OFF_TYPE;
    public static final String DIST_TRAVELED = SHAPE_DIST_TRAVELED;
    public static final String DIST = DIST_TRAVELED;

    public static final String PICK = PICKUP;
    public static final String DROP = DROP_OFF;
}
