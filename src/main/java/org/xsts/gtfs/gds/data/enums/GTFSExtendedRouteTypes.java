/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

import java.util.HashMap;

public class GTFSExtendedRouteTypes {
    static private GTFSExtendedRouteTypes instance = null;
    private HashMap<String, String> stringRegistry;
    private HashMap<Integer, String> integerRegistry;

    public String valueToString(Integer key) {
        if (integerRegistry.containsKey(key)){
            return integerRegistry.get(key);
        }
        return null;
    }

    static GTFSExtendedRouteTypes instance() {
        if ( instance == null ) {
            synchronized (GTFSExtendedRouteTypes.class) {
                if ( instance == null ) {
                    instance = new GTFSExtendedRouteTypes();
                }
            }
        }
        return instance;
    }

    private GTFSExtendedRouteTypes() {
        stringRegistry = new HashMap<>();
        integerRegistry = new HashMap<>();
        registerExtendedTypes();
    }


    private void registerExtendedTypes() {
        registeredClassOneTypes();
        registeredClassTwoTypes();
        registeredClassFourTypes();
        registeredClassSevenTypes();
        registeredClassEightTypes();
        registeredClassNineTypes();
        registeredClassThousandTypes();


    }
    private void registerInternal(Integer key, String description) {
        stringRegistry.put(key.toString(), description);
        integerRegistry.put(key, description);
    }

    private void registeredClassOneTypes() {
        registerInternal(100,	"Railway Service");
        registerInternal(101,	"High Speed Rail Service");
        registerInternal(102,	"Long Distance Trains");
        registerInternal(103,	"Inter Regional Rail Service");
        registerInternal(104,	"Car Transport Rail Service");
        registerInternal(105,	"Sleeper Rail Service");
        registerInternal(106,	"Regional Rail Service");
        registerInternal(107,	"Tourist Railway Service");
        registerInternal(108,	"Rail Shuttle (Within Complex)");
        registerInternal(109,	"Suburban Railway");
        registerInternal(110,	"Replacement Rail Service");
        registerInternal(111,	"Special Rail Service");
        registerInternal(112,	"Lorry Transport Rail Service");
        registerInternal(113,	"All Rail Services");
        registerInternal(114,	"Cross-Country Rail Service");
        registerInternal(115,	"Vehicle Transport Rail Service");
        registerInternal(116,	"Rack and Pinion Railway");
        registerInternal(117,	"Additional Rail Service");
    }



    private void registeredClassTwoTypes() {
        registerInternal(200,	"Coach Service");
        registerInternal(201,	"International Coach Service");
        registerInternal(202,	"National Coach Service");
        registerInternal(203,	"Shuttle Coach Service");
        registerInternal(204,	"Regional Coach Service");
        registerInternal(205,	"Special Coach Service");
        registerInternal(206,	"Sightseeing Coach Service");
        registerInternal(207,	"Tourist Coach Service");
        registerInternal(208,	"Commuter Coach Service");
        registerInternal(209,	"All Coach Services");
    }

    private void registeredClassFourTypes() {
        registerInternal(400,	"Urban Railway Service");
        registerInternal(401,	"Metro Service");
        registerInternal(402,	"Underground Service");
        registerInternal(403,	"Urban Railway Service");
        registerInternal(404,	"All Urban Railway Services");
        registerInternal(405,	"Monorail");
    }

    private void registeredClassSevenTypes() {
        registerInternal(700,	"Bus Service");
        registerInternal(701,	"Regional Bus Service");
        registerInternal(702,	"Express Bus Service");
        registerInternal(703,	"Stopping Bus Service");
        registerInternal(704,	"Local Bus Service");
        registerInternal(705,	"Night Bus Service");
        registerInternal(706,	"Post Bus Service");
        registerInternal(707,	"Special Needs Bus");
        registerInternal(708,	"Mobility Bus Service");
        registerInternal(709,	"Mobility Bus for Registered Disabled");
        registerInternal(710,	"Sightseeing Bus");
        registerInternal(711,	"Shuttle Bus");
        registerInternal(712,	"School Bus");
        registerInternal(713,	"School and Public Service Bus");
        registerInternal(714,	"Rail Replacement Bus Service");
        registerInternal(715,	"Demand and Response Bus Service");
        registerInternal(716,	"All Bus Services");
    }

    private void registeredClassEightTypes() {
        registerInternal(800,	"Trolleybus Service");

    }

    private void registeredClassNineTypes() {
        registerInternal(900,	"Tram Service");
        registerInternal(901,	"City Tram Service");
        registerInternal(902,	"Local Tram Service");
        registerInternal(903,	"Regional Tram Service");
        registerInternal(904,	"Sightseeing Tram Service");
        registerInternal(905,	"Shuttle Tram Service");
        registerInternal(906,	"All Tram Services");
    }

    private void registeredClassThousandTypes() {
        registerInternal(1000,	"Water Transport Service");
        registerInternal(1100,	"Air Service");
        registerInternal(1200,	"Ferry Service");
        registerInternal(1300,	"Aerial Lift Service");
        registerInternal(1400,	"Funicular Service");
        registerInternal(1500,	"Taxi Service");
        registerInternal(1501,	"Communal Taxi Service");
        registerInternal(1502,	"Water Taxi Service");
        registerInternal(1503,	"Rail Taxi Service");
        registerInternal(1504,	"Bike Taxi Service");
        registerInternal(1505,	"Licensed Taxi Service");
        registerInternal(1506,	"Private Hire Service Vehicle");
        registerInternal(1507,	"All Taxi Services");
        registerInternal(1700,	"Miscellaneous Service");
        registerInternal(1702,	"Horse-drawn Carriage");
    }
}

