/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSAttributionsFields;

import java.util.Map;

public class GTFSAttribution extends GTFSInfo {
    private String id;
    private String agency;
    private String route;
    private String trip;
    private String organization;
    private Boolean producer;
    private Boolean operator;
    private Boolean authority;
    private String url;
    private String email;
    private String phone;

    public GTFSAttribution(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        id          = getStringFieldOrNull(record, positions, GTFSAttributionsFields.ID );
        agency      = getStringFieldOrNull(record, positions, GTFSAttributionsFields.AGENCY );
        route   = getStringFieldOrNull(record, positions, GTFSAttributionsFields.ROUTE );
        trip    = getStringFieldOrNull(record, positions, GTFSAttributionsFields.TRIP );
        organization = getStringFieldOrNull(record, positions, GTFSAttributionsFields.ORGANIZATION );
        producer        = getBooleanFieldOrNull(record, positions, GTFSAttributionsFields.PRODUCER );
        operator         = getBooleanFieldOrNull(record, positions, GTFSAttributionsFields.OPERATOR );
        authority       = getBooleanFieldOrNull(record, positions, GTFSAttributionsFields.AUTHORITY );
        url   = getStringFieldOrNull(record, positions, GTFSAttributionsFields.URL );
        email       = getStringFieldOrNull(record, positions, GTFSAttributionsFields.EMAIL );
        phone       = getStringFieldOrNull(record, positions, GTFSAttributionsFields.PHONE );
    }

    public String id() { return id;}
    public String agency() { return agency;}
    public String route() { return route;}
    public String trip() { return trip;}
    public String organization() { return organization;}
    public Boolean producer() { return producer;}
    public Boolean operator() { return operator;}
    public Boolean authority() { return authority;}
    public String url() { return url;}
    public String email() { return email;}
    public String phone() { return phone;}

    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( id != null && id.length() > 0){
            sb.append("ID: " ).append(id).append("   ");
        }

        if ( agency != null && agency.length() > 0){
            sb.append("agency: " ).append(agency).append("   ");
        }

        if ( route != null && route.length() > 0){
            sb.append("route: " ).append(route).append("   ");
        }

        if ( trip != null && trip.length() > 0){
            sb.append("trip: " ).append(trip).append("   ");
        }

        if ( organization != null ){
            sb.append("organization: " ).append(organization).append("   ");
        }

        if ( producer != null ){
            sb.append("producer: " ).append(producer).append("   ");
        }

        if ( authority != null ){
            sb.append("authority: " ).append(authority).append("   ");
        }

        if ( url != null && url.length() > 0){
            sb.append("url: " ).append(url).append("   ");
        }

        if ( email != null && email.length() > 0){
            sb.append("Email: " ).append(email).append("   ");
        }

        if ( phone != null && phone.length() > 0){
            sb.append("phone: " ).append(phone).append("   ");
        }

        return sb.toString();
    }
}
