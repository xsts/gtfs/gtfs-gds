/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSLevelFields {
    public static final String LEVEL_ID = "level_id";
    public static final String LEVEL_INDEX = "level_index";
    public static final String LEVEL_NAME = "level_name";


    public static final String ID = LEVEL_ID;
    public static final String INDEX = LEVEL_INDEX;
    public static final String NAME = LEVEL_NAME;

}
