/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.timesheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class TOStationList {
    private List<TOStation> stations;

    public enum  SortMode {
        NAME,
        TIME_OFFSET,
        ORDER;
    }

    public TOStationList(){
        this.stations = new ArrayList<>();
    }

    public void add(TOStation station){
        this.stations.add(station);
    }

    public List<TOStation> getStations(){ return this.stations;}

    public TOStationList filterByNames(Set<String> filter) {
        TOStationList filteredStationList = new TOStationList();
        for (TOStation station: stations){
            if (! filter.contains(station.name().trim()))
                continue;
            TOStation offsetStation = new TOStation(station.name().trim(),
                    station.timeOffset() );
            filteredStationList.add(offsetStation);
        }
        return filteredStationList;
    }

    public void sort(SortMode sortMode){
        if (sortMode == SortMode.NAME) {
            internalSortByName();
        } else if (sortMode == SortMode.TIME_OFFSET) {
            internalSortByTimeOffset();
        } else if (sortMode == SortMode.ORDER) {
            internalSortByOrder();
        }
    }

    private void internalSortByName() {
        Comparator<TOStation> comparator = new Comparator<TOStation>(){
            @Override
            public int compare(final TOStation o1, final TOStation o2){
                return o1.name().compareTo(o2.name());
            }
        };
        Collections.sort(stations, comparator);

    }

    private void internalSortByTimeOffset() {
        Comparator<TOStation> comparator = new Comparator<TOStation>(){
            @Override
            public int compare(final TOStation o1, final TOStation o2){
                return o1.timeOffset().compareTo(o2.timeOffset());
            }
        };
        Collections.sort(stations, comparator);
    }

    private void internalSortByOrder() {
        Comparator<TOStation> comparator = new Comparator<TOStation>(){
            @Override
            public int compare(final TOStation o1, final TOStation o2){
                return o1.order() - o2.order();
            }
        };
        Collections.sort(stations, comparator);

    }

    public void print(){
        boolean first = true;
        System.out.println("[ ");
        for (TOStation station: stations){
            if (!first)
                System.out.println(",");
            System.out.print(station);
            first = false;
        }
        System.out.println("");
        System.out.println("]");
    }

    public TOStationList getOffsetList(Integer offset) {
        TOStationList offsetList = new TOStationList();
        for (TOStation station: stations){
            TOStation offsetStation = new TOStation(station.name(),
                    station.timeOffset() - offset);
            offsetStation.latitude(station.latitude());
            offsetStation.longitude(station.longitude());
            offsetList.add(offsetStation);
        }

        return offsetList;
    }

    private int getStationPositionByOffset(int offset){
        int stationPos = -1;
        int currentPos = -1;
        for ( TOStation station: stations){
            currentPos++;
            if (station.timeOffset() == offset){
                stationPos = currentPos;
                break;
            }
        }
        return stationPos;
    }

    public TOStationList getCenteredList(int count){
        TOStationList centeredList = new TOStationList();
        int zeroPos = getStationPositionByOffset(0);
        int half = count/2;
        int leftPos = zeroPos - half;
        int rightPos = zeroPos + half;
        if ( leftPos < 0){
            rightPos -= leftPos;
            leftPos = 0;
        } else if (rightPos >= stations.size()){
            leftPos += stations.size()-1-rightPos;
            rightPos = stations.size()-1;
        }

        int currentPos = -1;
        for ( TOStation station: stations) {
            currentPos++;
            if (  currentPos >= leftPos && currentPos <=rightPos){
                centeredList.add(station);
            }
        }

        return centeredList;
    }


    public int size()  { return stations.size();}
}
