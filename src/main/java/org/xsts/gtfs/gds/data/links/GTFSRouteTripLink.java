/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.links;

import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.HashMap;
import java.util.Map;

public class GTFSRouteTripLink {

    private Map<String, GTFSTripToStopTimeLink> link = null;
    private String longestTrip = null;

    public GTFSRouteTripLink() {
        link = new HashMap<>();
    }

    public void add( String tripID) {
        if (link.containsKey(tripID) == false) {
            link.put(tripID, new GTFSTripToStopTimeLink());
        }

    }

    public void add( String tripID, GTFSStopTime stopTime) {
        if (link.containsKey(tripID) == false) {
            link.put(tripID, new GTFSTripToStopTimeLink());
        }
        GTFSTripToStopTimeLink tripLink = link.get(tripID);
        tripLink.add(stopTime);
        if (longestTrip == null)
            longestTrip = tripID;
    }

    public Map< String, GTFSTripToStopTimeLink> getLink() {
        return link;
    }


    public void checkAndPurge() {

        //Runtime.getRuntime().gc();
        if (longestTrip != null) {
            GTFSTripToStopTimeLink longest = link.get(longestTrip);
            int sizeRecord = longest.getLink().size();

            for ( String key: link.keySet()){
                if ( key.compareTo(longestTrip) == 0)
                    continue;
                GTFSTripToStopTimeLink tripLink = link.get(key);
                int size =  tripLink.getLink().size();

                if ( size <= sizeRecord ) {
                    tripLink.prune();
                } else if (size > sizeRecord) {
                    sizeRecord = size;
                    longestTrip = key;
                    longest.prune();
                    longest = link.get(longestTrip);
                }

            }
        }



    }
}
