/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.names;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSFileNames {
    // Required files
    public static final String AGENCY = "agency.txt";
    public static final String ROUTES = "routes.txt";
    public static final String STOP_TIMES = "stop_times.txt";
    public static final String STOPS = "stops.txt";
    public static final String TRIPS = "trips.txt";

    // Conditional required files
    public static final String CALENDAR = "calendar.txt";
    public static final String CALENDAR_DATES = "calendar_dates.txt";
    public static final String FEED_INFO = "feed_info.txt";

    // Optional files
    public static final String ATTRIBUTIONS = "attributions.txt";
    public static final String FARE_ATTRIBUTES = "fare_attributes.txt";
    public static final String FARE_RULES = "fare_rules.txt";
    public static final String LEVELS = "levels.txt";
    public static final String PATHWAYS = "pathways.txt";
    public static final String SHAPES = "shapes.txt";
    public static final String TRANSFERS = "transfers.txt";
    public static final String TRANSLATIONS = "translations.txt";
}
