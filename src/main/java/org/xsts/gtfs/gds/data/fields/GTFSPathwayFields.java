/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSPathwayFields {
    public static final String PATHWAY_ID = "pathway_id";
    public static final String FROM_STOP_ID = "from_stop_id";
    public static final String TO_STOP_ID = "to_stop_id";
    public static final String PATHWAY_MODE = "pathway_mode";
    public static final String IS_BIDIRECTIONAL = "is_bidirectional";
    public static final String LENGTH = "length";
    public static final String TRAVERSAL_TIME = "traversal_time";
    public static final String STAIR_COUNT = "stair_count";
    public static final String MAX_SLOPE = "max_slope";
    public static final String MIN_WIDTH = "min_width";
    public static final String SIGNPOSTED_AS = "signposted_as";
    public static final String REVERSED_SIGNPOSTED_AS = "reversed_signposted_as";

    public static final String ID = PATHWAY_ID;
    public static final String FROM_STOP = FROM_STOP_ID;
    public static final String TO_STOP = TO_STOP_ID;
    public static final String MODE = PATHWAY_MODE;
    public static final String BIDIRECTIONAL = IS_BIDIRECTIONAL;
    public static final String TIME = TRAVERSAL_TIME;
    public static final String SLOPE = MAX_SLOPE;
    public static final String WIDTH = MIN_WIDTH;
    public static final String SIGNPOSTED = SIGNPOSTED_AS;
    public static final String REVERSED_SIGNPOSTED = REVERSED_SIGNPOSTED_AS;

    public static final String FROM = FROM_STOP;
    public static final String TO = TO_STOP;

}

