/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSDirection {
    public static final Integer OUTBOUND = 0;
    public static final Integer INBOUND = 1;

}
