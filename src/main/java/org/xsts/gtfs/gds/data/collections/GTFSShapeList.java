/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.LinearValueList;
import org.xsts.gtfs.gds.data.types.GTFSShape;

import java.util.ArrayList;
import java.util.List;

public class GTFSShapeList implements LinearValueList {

    List<GTFSShape> shapeSegments;
    public GTFSShapeList() {
        shapeSegments = new ArrayList<>();
    }

    public void add(GTFSShape shapeSegment) {
        shapeSegments.add(shapeSegment);
    }

    public List<GTFSShape> getAll() { return shapeSegments;}
    @Override
    public void add(Object o) {
        shapeSegments.add( (GTFSShape) o);
    }

    @Override
    public Object getAt(int i) {
        return shapeSegments.get(i);
    }

    @Override
    public boolean isEmpty() {
        return shapeSegments.size() == 0;
    }
}
