/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSTripFields {
    public static final String ROUTE_ID = "route_id";
    public static final String SERVICE_ID = "service_id";
    public static final String TRIP_ID = "trip_id";
    public static final String TRIP_HEADSIGN = "trip_headsign";
    public static final String TRIP_SHORT_NAME = "trip_short_name";
    public static final String DIRECTION_ID = "direction_id";
    public static final String BLOCK_ID = "block_id";
    public static final String SHAPE_ID = "shape_id";
    public static final String WHEELCHAIR_ACCESSIBLE = "wheelchair_accessible";
    public static final String BIKES_ALLOWED = "bikes_allowed";

    public static final String ROUTE = ROUTE_ID;
    public static final String SERVICE = SERVICE_ID;
    public static final String TRIP = TRIP_ID;
    public static final String HEADSIGN = TRIP_HEADSIGN;
    public static final String SHORT_NAME = TRIP_SHORT_NAME;
    public static final String DIRECTION = DIRECTION_ID;
    public static final String BLOCK = BLOCK_ID;
    public static final String SHAPE = SHAPE_ID;
    public static final String WHEELCHAIR = WHEELCHAIR_ACCESSIBLE;
    public static final String ACCESSIBLE = WHEELCHAIR_ACCESSIBLE;
    public static final String BIKES = BIKES_ALLOWED;

    public static final String ID = TRIP_ID;


}

