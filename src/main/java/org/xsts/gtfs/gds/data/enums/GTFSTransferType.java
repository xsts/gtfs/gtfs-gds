/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSTransferType {
    public static final Integer RECOMMENDED = 0;
    public static final Integer SYNCHRONIZED_VEHICLES = 1;
    public static final Integer MIN_REQUIRED = 2;
    public static final Integer NOT_ALLOWED = 3;
    public static final Integer RECOMMENDED_BIS = null;

}
