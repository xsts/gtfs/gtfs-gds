/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSDropOffType {
    public static final Integer CONTINUOUS = 0;
    public static final Integer UNAVAILABLE = 1;
    public static final Integer CONTACT_AGENCY = 2;
    public static final Integer CONTACT_DRIVER = 3;

    public static String valueToString(String value){
        if (value == null)
            return null;

        Integer intValue = Integer.parseInt(value);
        return valueToString(intValue);
    }

    public static String valueToString(Integer value) {
        if(value == CONTINUOUS)
            return TEXT_CONTINUOUS;

        if(value == UNAVAILABLE)
            return TEXT_UNAVAILABLE;

        if(value == CONTACT_AGENCY)
            return TEXT_CONTACT_AGENCY;

        if(value == CONTACT_DRIVER)
            return TEXT_CONTACT_DRIVER;

        return null;
    }

    public static final String TEXT_CONTINUOUS = "Continuous";
    public static final String TEXT_UNAVAILABLE = "Unavailable";
    public static final String TEXT_CONTACT_AGENCY = "Contact the agency";
    public static final String TEXT_CONTACT_DRIVER = "Contact the driver";
}
