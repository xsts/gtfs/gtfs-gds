/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.LinearValueList;
import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GTFSStopTimeLinearList implements LinearValueList {
    List<GTFSStopTime> stopTimes;

    public GTFSStopTimeLinearList() {
        stopTimes = new ArrayList<>();
    }
    public  void add(GTFSStopTime stopTime){
        stopTimes.add(stopTime);
    }

    public List<GTFSStopTime> getAll() { return stopTimes; }


    @Override
    public void add(Object object) {
        stopTimes.add((GTFSStopTime) object);
    }

    @Override
    public Object getAt(int pos) {
        return stopTimes.get(pos);
    }

    @Override
    public boolean isEmpty() {
        return stopTimes.size() == 0;
    }

    public Set<String> stopIDList() {
        Set<String> theStops = new HashSet<>();
        for (GTFSStopTime stopTime : stopTimes) {
            theStops.add(stopTime.stop());
        }

        return theStops;
    }
}