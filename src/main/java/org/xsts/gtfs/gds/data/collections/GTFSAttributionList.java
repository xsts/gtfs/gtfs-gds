/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gds.data.types.GTFSAttribution;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GTFSAttributionList implements KeyValueList {
    private Map<String, GTFSAttribution> attributions;

    public GTFSAttributionList(){
        attributions = new HashMap<>();
    }

    @Override
    public void add(String id, Object object) {
        add(id, (GTFSAttribution) object);
    }
    public void add(String id, GTFSAttribution attribution){
        attributions.put(id, attribution);
    }
    public GTFSAttribution get(String id) {
        return attributions.get(id);
    }
    @Override
    public boolean isEmpty() { return attributions.size() == 0;}

    public GTFSAttribution getFirstAttribution() {
        return attributions.get(attributions.keySet().toArray()[0]);
    }
    public Set<String> keySet() { return attributions.keySet(); }
    public int count() { return attributions.size();}
}
