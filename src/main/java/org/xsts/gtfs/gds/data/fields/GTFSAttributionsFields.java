/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSAttributionsFields {
    public static final String ATTRIBUTION_ID       = "attribution_id";
    public static final String AGENCY_ID            = "agency_id";
    public static final String ROUTE_ID             = "route_id";
    public static final String TRIP_ID              = "trip_id";
    public static final String ORGANIZATION_NAME    = "organization_name";
    public static final String IS_PRODUCER          = "is_producer";
    public static final String IS_OPERATOR          = "is_operator";
    public static final String IS_AUTHORITY         = "is_authority";
    public static final String ATTRIBUTION_URL      = "attribution_url";
    public static final String ATTRIBUTION_EMAIL    = "attribution_email";
    public static final String ATTRIBUTION_PHONE    = "attribution_phone";

    public static final String ID = ATTRIBUTION_ID;
    public static final String AGENCY = AGENCY_ID;
    public static final String ROUTE = ROUTE_ID;
    public static final String TRIP = TRIP_ID;
    public static final String ORGANIZATION = ORGANIZATION_NAME;
    public static final String PRODUCER = IS_PRODUCER;
    public static final String OPERATOR = IS_OPERATOR;
    public static final String AUTHORITY = IS_AUTHORITY;
    public static final String URL = ATTRIBUTION_URL;
    public static final String EMAIL = ATTRIBUTION_EMAIL;
    public static final String PHONE = ATTRIBUTION_PHONE;

}
