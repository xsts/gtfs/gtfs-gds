/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.util;

import java.util.*;

public class GTFSRouteSorter {
    List<String> sortedRoutes = null;
    List<String> routesNumber = null;
    List<String> routesNumberAlpha = null;
    List<String> routesAlphaNumber = null;
    List<String> routesAlpha = null;
    GTFSRouteNameClassifier classifier = null;



    public GTFSRouteSorter(Set<String> keys) {
        resetLists();
        distributeKeys(keys);
        separateSort();
        joinLists();
    }

    private void joinLists() {
        for (String s:routesNumber )
            sortedRoutes.add(s);
        for (String s:routesNumberAlpha )
            sortedRoutes.add(s);
        for (String s:routesAlpha )
            sortedRoutes.add(s);
        for (String s:routesAlphaNumber )
            sortedRoutes.add(s);
    }

    public List<String> getSortedList() { return this.sortedRoutes; }

    private void separateSort() {
        sortNumbers();
        sortAlphas();
        sortNumberAlphas();
        sortAlphaNumbers();
    }

    private void sortNumbers() {
        Comparator<String> comparator = new Comparator<String>(){
            @Override
            public int compare(final String o1, final String o2){
                Integer i1 = Integer.parseInt(o1);
                Integer i2 = Integer.parseInt(o2);
                if (i1 > i2)
                    return 1;
                else if (i1 < i2)
                    return -1;
                else
                    return 0;
            }
        };
        Collections.sort(routesNumber, comparator);
    }

    private void sortAlphas() {
        Comparator<String> comparator = new Comparator<String>(){
            @Override
            public int compare(final String o1, final String o2){
                String s1 = o1.toLowerCase();
                String s2 = o2.toLowerCase();
                return s1.compareTo(s2);
            }
        };
        Collections.sort(routesAlpha, comparator);
    }

    private void sortNumberAlphas() {
        Comparator<String> comparator = new Comparator<String>(){
            @Override
            public int compare(final String o1, final String o2){

                String s1 = o1.toLowerCase();
                String s2 = o2.toLowerCase();

                Integer i1 = Integer.parseInt(getNumberPart(s1));
                Integer i2 = Integer.parseInt(getNumberPart(s2));
                if (i1 > i2)
                    return 1;
                else if (i1 < i2)
                    return -1;
                else {
                    String a1 = getAlphaPart(s1);
                    String a2 = getAlphaPart(s2);
                    return a1.compareTo(a2);
                }
            }
        };
        Collections.sort(routesNumberAlpha, comparator);
    }

    private void sortAlphaNumbers() {
        Comparator<String> comparator = new Comparator<String>(){
            @Override
            public int compare(final String o1, final String o2){

                String s1 = o1.toLowerCase();
                String s2 = o2.toLowerCase();

                String a1 = getAlphaPart(s1);
                String a2 = getAlphaPart(s2);
                int stringBalance = a1.compareTo(a2);
                if ( stringBalance != 0)
                    return stringBalance;
                else {
                    Integer i1 = Integer.parseInt(getNumberPart(s1));
                    Integer i2 = Integer.parseInt(getNumberPart(s2));
                    if (i1 > i2)
                        return 1;
                    else if (i1 < i2)
                        return -1;
                    else
                        return 0;
                }
            }
        };
        Collections.sort(routesAlphaNumber, comparator);
    }

    private String getAlphaPart(String name){
        StringBuilder sb = new StringBuilder();
        for (char c: name.toCharArray()){
            if (GTFSRouteNameClassifier.isAlpha(c))
                sb.append(c);
        }
        return sb.toString();
    }

    private String getNumberPart(String name){
        StringBuilder sb = new StringBuilder();
        for (char c: name.toCharArray()){
            if (GTFSRouteNameClassifier.isDigit(c))
                sb.append(c);
        }
        return sb.toString();
    }

    private void distributeKeys(Set<String> keys) {
        for (String key: keys) {
            int  type = classifier.detectType(key);
            switch(type){
                case GTFSRouteNameClassifier.NUMBER:
                    routesNumber.add(key);
                    break;
                case GTFSRouteNameClassifier.ALPHA:
                    routesAlpha.add(key);
                    break;
                case GTFSRouteNameClassifier.NUMBER_ALPHA:
                    routesNumberAlpha.add(key);
                    break;
                case GTFSRouteNameClassifier.ALPHA_NUMBER:
                    routesAlphaNumber.add(key);
                    break;
                default:
                    break; // skip others
            }
        }
    }

    private void resetLists() {
        sortedRoutes = new ArrayList<>();
        routesNumber = new ArrayList<>();
        routesNumberAlpha = new ArrayList<>();
        routesAlpha = new ArrayList<>();
        routesAlphaNumber = new ArrayList<>();
        classifier = new GTFSRouteNameClassifier();
    }
}
