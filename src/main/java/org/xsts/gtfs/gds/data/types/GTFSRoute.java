/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.enums.GTFSDropOffType;
import org.xsts.gtfs.gds.data.enums.GTFSPickUpType;
import org.xsts.gtfs.gds.data.enums.GTFSRouteType;
import org.xsts.gtfs.gds.data.fields.GTFSRouteFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSRoute {
    private String uid;

    private String id;
    private String agency;
    private String shortName;
    private String longName;
    private String description;
    private String type;
    private String url;
    private String color;
    private String textColor;
    private String sortOrder;
    private String pickUp;
    private String dropOff;

    public GTFSRoute(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();

        if (positions.containsKey(GTFSRouteFields.ID))
            id = record.get(positions.get(GTFSRouteFields.ID)).trim();

        if (positions.containsKey(GTFSRouteFields.AGENCY))
            agency = record.get(GTFSRouteFields.AGENCY).trim();

        if (positions.containsKey(GTFSRouteFields.SHORT_NAME))
            shortName = record.get(GTFSRouteFields.SHORT_NAME).trim();

        if (positions.containsKey(GTFSRouteFields.LONG_NAME))
            longName = record.get(GTFSRouteFields.LONG_NAME).trim();

        if (positions.containsKey(GTFSRouteFields.DESC))
            description = record.get(GTFSRouteFields.DESC).trim();

        if (positions.containsKey(GTFSRouteFields.TYPE))
            type = record.get(GTFSRouteFields.TYPE).trim();

        if (positions.containsKey(GTFSRouteFields.URL))
            url = record.get(GTFSRouteFields.URL).trim();

        if (positions.containsKey(GTFSRouteFields.COLOR))
            color = record.get(GTFSRouteFields.COLOR).trim();

        if (positions.containsKey(GTFSRouteFields.TEXT_COLOR))
            textColor = record.get(GTFSRouteFields.TEXT_COLOR).trim();

        if (positions.containsKey(GTFSRouteFields.SORT_ORDER))
            sortOrder = record.get(GTFSRouteFields.SORT_ORDER).trim();

        if (positions.containsKey(GTFSRouteFields.PICKUP))
            pickUp = record.get(GTFSRouteFields.PICKUP);

        if (positions.containsKey(GTFSRouteFields.DROP_OFF))
            dropOff = record.get(GTFSRouteFields.DROP_OFF);

        regenerateMissingInformation();
    }

    private void regenerateMissingInformation() {
        if (shortName != null ||  longName != null || description != null) {
            String regeneratedInfo = "";
            if ( shortName != null && shortName.length() > 0)
                regeneratedInfo = shortName;
            else if ( longName != null && longName.length() > 0)
                regeneratedInfo = longName;
            else if ( description != null && description.length() > 0)
                regeneratedInfo = description;

            if ( shortName == null || shortName.length() == 0)
                shortName = regeneratedInfo;
            if ( longName == null || longName.length() == 0)
                longName = regeneratedInfo;
            if ( description == null || description.length() == 0)
                description = regeneratedInfo;
        }
    }

    public String id() { return id;}
    public String agency() { return agency;}
    public String shortName() { return shortName;}
    public String longName() { return longName;}
    public String description() { return description;}
    public String type() { return type;}
    public String url() { return url;}
    public String color() { return color;}
    public String textColor() { return textColor;}
    public String sortOrder() { return sortOrder;}
    public String pickUp() { return pickUp;}
    public String dropOff() { return dropOff;}

    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( id != null && id.length() > 0){
            sb.append("ID: " ).append(id).append("   ");
        }

        if ( agency != null && agency.length() > 0){
            sb.append("Agency ID: " ).append(agency).append("   ");
        }

        if ( shortName != null && shortName.length() > 0){
            sb.append("Short Name: " ).append(shortName).append("   ");
        }

        if ( longName != null && longName.length() > 0){
            sb.append("Long Name: " ).append(longName).append("   ");
        }

        if ( description != null && description.length() > 0){
            sb.append("Description: " ).append(description).append("   ");
        }

        if ( type != null && type.length() > 0){
            sb.append("Type: \"" ).append(GTFSRouteType.valueToString(type)).append("\"   ");
        }

        if ( url != null && url.length() > 0){
            sb.append("URL: \"" ).append(url).append("\"   ");
        }

        if ( color != null && color.length() > 0){
            sb.append("Color: " ).append(color).append("   ");
        }

        if ( textColor != null && textColor.length() > 0){
            sb.append("Text Color: " ).append(textColor).append("   ");
        }

        if ( sortOrder != null && sortOrder.length() > 0){
            sb.append("Sort Order: " ).append(sortOrder).append("   ");
        }

        if ( pickUp != null && pickUp.length() > 0){
            sb.append("Pick Up: \"" ).append(GTFSPickUpType.valueToString(pickUp)).append("\"   ");
        }

        if ( dropOff != null && dropOff.length() > 0){
            sb.append("Drop Off: \"" ).append(GTFSDropOffType.valueToString(dropOff)).append("\"   ");
        }

        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerRoute();
    }
}
