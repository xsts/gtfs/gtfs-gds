/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.memcache;

import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.gtfs.gds.data.collections.GTFSShapeList;
import org.xsts.gtfs.gds.data.collections.GTFSShapeListMap;
import org.xsts.gtfs.gds.data.collections.GTFSStopList;
import org.xsts.gtfs.gds.data.collections.GTFSUniqueCodeIdentifierList;
import org.xsts.gtfs.gds.data.links.GTFSRouteTripLink;
import org.xsts.gtfs.gds.data.links.GTFSTripToStopTimeLink;
import org.xsts.gtfs.gds.data.types.GTFSStop;
import org.xsts.gtfs.gds.data.types.GTFSStopTime;
import org.xsts.gtfs.gds.data.types.GTFSUniqueCodeIdentifier;

import java.util.*;

public class GTFSCache {
    public static final String CACHE_SIGNATURE = "bc.signature";
    Map<String, GTFSRouteTripLink> link = null;
    Map<String, String> reverseLink = null;
    GTFSUniqueCodeIdentifierList uids = null;

    Map < String, Map < String, String>> tripStopSequences = null;
    Map<String, String> tripSignatures = null;
    Map<String, String> reverseTripSignatures = null;

    GTFSShapeListMap shapeListMap = null;
    Map<String, String> tripShapeLink = null;

    String lastTrip = null;

    public GTFSCache() {
        link = new HashMap<>(); // route -> trip list
        reverseLink = new HashMap<>(); // trip -> route
        uids = new GTFSUniqueCodeIdentifierList();
        tripStopSequences = new HashMap<>();
        tripSignatures = new HashMap<>();
        reverseTripSignatures = new HashMap<>();
        shapeListMap = new GTFSShapeListMap();
        tripShapeLink = new HashMap<>();
    }

    public void makeGlobal(){
        EnvironmentConfiguration config = EnvironmentConfiguration.getInstance();
        config.put(CACHE_SIGNATURE,this);
    }

    public static Boolean hasGlobal() {
        EnvironmentConfiguration config = EnvironmentConfiguration.getInstance();
        return config.contains(CACHE_SIGNATURE);
    }
    public static GTFSCache fromGlobal() {
        EnvironmentConfiguration config = EnvironmentConfiguration.getInstance();
        GTFSCache bigCache = (GTFSCache)config.get(CACHE_SIGNATURE);
        return bigCache;
    }

    public void add(String routeID) {
        if ( link.containsKey(routeID) == false) {
            GTFSRouteTripLink routeLink = new GTFSRouteTripLink();
            link.put(routeID, routeLink);
        }
    }

    public void add(String routeID, String tripID) {
        GTFSRouteTripLink routeLink = link.get(routeID);
        routeLink.add(tripID);
        reverseLink.put(tripID,routeID );
    }

    public void add(String routeID, String tripID, GTFSStopTime stopTime) {
        GTFSRouteTripLink routeLink = link.get(routeID);

        if ( lastTrip != null && lastTrip.compareTo(tripID) != 0){
            routeLink.checkAndPurge();

            // purge unnecessary memory


        }

        GTFSTripToStopTimeLink tripLink = routeLink.getLink().get(tripID);
        tripLink.add(stopTime);
        //  routeLink.add(tripID);
        lastTrip = tripID;
    }


    public void addTripShapeLink(String tripID, String shapeID) {
        tripShapeLink.put(tripID, shapeID);
    }


    public void createShapeList(String shapeID) {
        if ( shapeListMap.contains(shapeID) == false) {
            GTFSShapeList shapeList = new GTFSShapeList();
            shapeListMap.add(shapeID, shapeList);
        }
    }

    public GTFSShapeList getShapeList(String shapeID) {
        return shapeListMap.get(shapeID);
    }

    public boolean shapeListExists(String shapeID) {
        return shapeListMap.contains(shapeID);
    }

    public GTFSShapeListMap getShapeListMap() {
        return shapeListMap;
    }

    public Map<String, String> getReverseLink() {
        return reverseLink;
    }

    public Map<String, GTFSRouteTripLink> getLink() {
        return link;
    }

    public Map<String, String> tripSignatures() {return this.tripSignatures; }

    public Map<String, String> getTripShapeLink() { return tripShapeLink;}

    public String registerStop(){
        GTFSUniqueCodeIdentifier uid = GTFSUniqueCodeIdentifier.generate(GTFSUniqueCodeIdentifier.STOP_TYPE);
        uids.add(uid.uid(), uid);
        return uid.uid();
    }

    public String registerAgency(){
        GTFSUniqueCodeIdentifier uid = GTFSUniqueCodeIdentifier.generate(GTFSUniqueCodeIdentifier.AGENCY_TYPE);
        uids.add(uid.uid(), uid);
        return uid.uid();
    }


    public String registerTrip(){
        GTFSUniqueCodeIdentifier uid = GTFSUniqueCodeIdentifier.generate(GTFSUniqueCodeIdentifier.TRIP_TYPE);
        uids.add(uid.uid(), uid);
        return uid.uid();
    }

    public String registerRoute(){
        GTFSUniqueCodeIdentifier uid = GTFSUniqueCodeIdentifier.generate(GTFSUniqueCodeIdentifier.ROUTE_TYPE);
        uids.add(uid.uid(), uid);
        return uid.uid();
    }

    public void registerTripStopSequence(String tripID, String stopUID, String sequence) {
        Map<String, String> stopSequence = null;
        if ( ! tripStopSequences.containsKey(tripID)) {
            stopSequence = new HashMap<>();
            tripStopSequences.put(tripID, stopSequence);
        } else {
            stopSequence = tripStopSequences.get(tripID);
        }

        stopSequence.put(sequence, stopUID);
    }

    public void generateTripSignatures(GTFSStopList stops) {
        Map<String, GTFSStop> stopKeys = new HashMap<>();
        for ( String stopKey: stops.getStops().keySet()){
            GTFSStop stop = stops.get(stopKey);
            stopKeys.put(stop.uid(), stop);
        }

        //tripSignatures
        for (String tripID: tripStopSequences.keySet()){
            Map<String, String>  stopSequence = tripStopSequences.get(tripID);
            List<Integer> sequence = new ArrayList<>();
            for ( String seq: stopSequence.keySet()){
                Integer intVal = Integer.parseInt(seq);
                sequence.add(intVal);
            }
            Comparator<Integer> comparator = new Comparator<Integer>(){
                @Override
                public int compare(final Integer o1, final Integer o2){
                    return o1.compareTo(o2);
                }
            };
            Collections.sort(sequence, comparator);
            StringBuilder sb = new StringBuilder();
            String sep = "";
            boolean mangle = true;
            for ( Integer intVal : sequence){
                sb.append(sep);
                if (!mangle) {
                    String key2 = stopSequence.get(intVal.toString());
                    String name = stopKeys.get(key2).name();
                    sb.append(name);
                    sep = " >>> ";
                } else {
                    String key2 = stopSequence.get(intVal.toString());
                    String name = stopKeys.get(key2).uid();
                    sb.append(name);
                    sep = "-";
                }



            }

            if ( !reverseTripSignatures.containsKey(sb.toString())) {
                tripSignatures.put(tripID, sb.toString());
                reverseTripSignatures.put(sb.toString(), tripID);

            }


        }
    }

    public String registerTransfer(){
        GTFSUniqueCodeIdentifier uid = GTFSUniqueCodeIdentifier.generate(GTFSUniqueCodeIdentifier.TRANSFER_TYPE);
        uids.add(uid.uid(), uid);
        return uid.uid();
    }



}
