/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSAgencyFields;
import org.xsts.gtfs.gds.data.fields.GTFSFeedInfoFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSFeedInfo extends GTFSInfo{
    private String uid;

    private String publisher;
    private String url;
    private String lang;
    private String defaultLang;
    private String start;
    private String end;
    private String version;
    private String email;
    private String contactUrl;

    public GTFSFeedInfo(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        publisher   = getStringFieldOrNull(record, positions, GTFSFeedInfoFields.PUBLISHER );
        url         = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.PUBLISHER_URL );
        lang        = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.LANG );
        defaultLang = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.DEFAULT_LANG );
        start       = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.START );
        end         = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.END );
        version     = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.VERSION );
        email       = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.EMAIL );
        contactUrl  = getStringFieldOrNull(record, positions,GTFSFeedInfoFields.CONTACT_URL );
    }

    public String publisher() { return publisher;}
    public String url() { return url;}
    public String lang() { return lang;}
    public String defaultLang() { return defaultLang;}

    public String start() { return start;}
    public String end() { return end;}
    public String version() { return version;}
    public String email() { return email;}

    public String contactUrl() { return contactUrl;}

    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( publisher != null && publisher.length() > 0){
            sb.append("ID: " ).append(publisher).append("   ");
        }

        if ( url != null && url.length() > 0){
            sb.append("URL: " ).append(url).append("   ");
        }

        if ( lang != null && lang.length() > 0){
            sb.append("Lang: " ).append(lang).append("   ");
        }

        if ( lang != null && lang.length() > 0){
            sb.append("Default lang: " ).append(defaultLang).append("   ");
        }

        if ( start != null && start.length() > 0){
            sb.append("Start date: " ).append(start).append("   ");
        }

        if ( end != null && end.length() > 0){
            sb.append("End date: " ).append(end).append("   ");
        }

        if ( version != null && version.length() > 0){
            sb.append("Version: " ).append(version).append("   ");
        }

        if ( email != null && email.length() > 0){
            sb.append("Contact Email: " ).append(email).append("   ");
        }
        if ( contactUrl != null && contactUrl.length() > 0){
            sb.append("Contact URL: " ).append(contactUrl).append("   ");
        }



        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerAgency();
    }
}
