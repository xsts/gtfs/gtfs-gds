/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSTimePointType {
    public static final Integer APPROXIMATE = 0;
    public static final Integer EXACT = 1;

}
