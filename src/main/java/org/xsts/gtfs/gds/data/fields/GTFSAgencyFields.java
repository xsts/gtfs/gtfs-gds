/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSAgencyFields {
    public static final String AGENCY_ID        = "agency_id";
    public static final String AGENCY_NAME      = "agency_name";
    public static final String AGENCY_URL       = "agency_url";
    public static final String AGENCY_TIMEZONE  = "agency_timezone";
    public static final String AGENCY_LANG      = "agency_lang";
    public static final String AGENCY_PHONE     = "agency_phone";
    public static final String AGENCY_FARE_URL  = "agency_fare_url";
    public static final String AGENCY_EMAIL     = "agency_email";

    public static final String ID        = AGENCY_ID;
    public static final String NAME     = AGENCY_NAME;
    public static final String URL      = AGENCY_URL;
    public static final String TIMEZONE = AGENCY_TIMEZONE;
    public static final String LANG     = AGENCY_LANG;
    public static final String PHONE    = AGENCY_PHONE;
    public static final String FARE_URL = AGENCY_FARE_URL;
    public static final String EMAIL    = AGENCY_EMAIL;
}


