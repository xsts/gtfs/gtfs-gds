/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.HashMap;
import java.util.Map;

public class GTFSStopTimeList {
    private Map<String, GTFSStopTime> stopTimes;

    public GTFSStopTimeList(){
        stopTimes = new HashMap<>();
    }

    private GTFSStopTimeList(Map<String, GTFSStopTime> strongReference) { this.stopTimes = strongReference; }

    public void add(String sequence, GTFSStopTime stopTime){
        stopTimes.put(sequence,stopTime);
    }

    public GTFSStopTime get(String sequence) {
        return stopTimes.get(sequence);
    }

    public Map<String, GTFSStopTime> getStopTimes() { return stopTimes;}

    public boolean isEmpty() { return stopTimes.size() == 0;};

    public static GTFSStopTimeList temporaryList(Map<String, GTFSStopTime> strongReference) {
        return new GTFSStopTimeList(strongReference);
    }
}