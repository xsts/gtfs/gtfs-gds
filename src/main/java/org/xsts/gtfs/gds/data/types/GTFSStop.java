/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSStopFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;
import java.util.regex.Pattern;

public class GTFSStop {
    private String uid;

    private String id;
    private String code;
    private String name;
    private String desc;
    private String lat;
    private String lon;
    private String zone;
    private String url;
    private String location;
    private String parent;
    private String timezone;
    private String wheelchair;
    private String level;
    private String platform;

    public GTFSStop(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        if (positions.containsKey(GTFSStopFields.ID))
            id = record.get(GTFSStopFields.ID).trim();

        if (positions.containsKey(GTFSStopFields.CODE))
            code = record.get(GTFSStopFields.CODE).trim();

        if (positions.containsKey(GTFSStopFields.NAME))
            name = record.get(GTFSStopFields.NAME).trim();

        if (positions.containsKey(GTFSStopFields.DESC))
            desc = record.get(GTFSStopFields.DESC).trim();

        if (positions.containsKey(GTFSStopFields.LAT))
            lat = record.get(GTFSStopFields.LAT).trim();

        if (positions.containsKey(GTFSStopFields.LON))
            lon = record.get(GTFSStopFields.LON).trim();

        if (positions.containsKey(GTFSStopFields.ZONE))
            zone = record.get(GTFSStopFields.ZONE).trim();

        if (positions.containsKey(GTFSStopFields.URL))
            url = record.get(GTFSStopFields.URL).trim();

        if (positions.containsKey(GTFSStopFields.LOCATION))
            location = record.get(GTFSStopFields.LOCATION).trim();

        if (positions.containsKey(GTFSStopFields.PARENT))
            parent = record.get(GTFSStopFields.PARENT).trim();

        if (positions.containsKey(GTFSStopFields.TIMEZONE))
            timezone = record.get(GTFSStopFields.TIMEZONE).trim();

        if (positions.containsKey(GTFSStopFields.WHEELCHAIR))
            wheelchair = record.get(GTFSStopFields.WHEELCHAIR).trim();

        if (positions.containsKey(GTFSStopFields.LEVEL))
            level = record.get(GTFSStopFields.LEVEL).trim();

        if (positions.containsKey(GTFSStopFields.PLATFORM))
            platform = record.get(GTFSStopFields.PLATFORM);
    }

    public String id() { return id;}
    public String code() { return code;}
    public String name() { return name;}
    public String description() { return desc;}
    public String latitude() { return lat;}
    public String longitude() { return lon;}
    public String zone() { return zone;}
    public String url() { return url;}
    public String location() { return location;}
    public String parent() { return parent;}
    public String timezone() { return timezone;}
    public String wheelchair() { return wheelchair;}
    public String level() { return level;}
    public String platform() { return platform;}

    public String getCleanName() {
        String cleanName = name.split(Pattern.quote("("))[0];
        return cleanName;
    }

    public String toString() {
        StringBuilder sb= new StringBuilder();
        sb.append("{ name: ").append(getCleanName()).append(", ");
        sb.append("lat: ").append(lat).append(", ");
        sb.append("lon: ").append(lon).append("}");
        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerStop();
    }
}
