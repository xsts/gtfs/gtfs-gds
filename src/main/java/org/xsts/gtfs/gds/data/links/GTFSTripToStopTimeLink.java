/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.links;

import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.HashMap;
import java.util.Map;

public class GTFSTripToStopTimeLink {
    private Map< String, GTFSStopTime> link = null;

    public GTFSTripToStopTimeLink(){
        link = new HashMap<>();
    }

    public void add(GTFSStopTime stopTime){
        link.put(stopTime.sequence(),stopTime);
    }
    public Map< String, GTFSStopTime> getLink() {
        return link;
    }
    public void prune() {
        link = new HashMap<>();
    }
}
