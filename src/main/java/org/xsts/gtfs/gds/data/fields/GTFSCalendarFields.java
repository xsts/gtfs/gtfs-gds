/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSCalendarFields {
    public static final String SERVICE_ID = "service_id";
    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
    public static final String SUNDAY = "sunday";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";

    public static final String SERVICE = SERVICE_ID;
    public static final String START = START_DATE;
    public static final String END = END_DATE;
}
