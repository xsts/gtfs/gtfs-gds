/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSTransferFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSTransfer extends GTFSInfo{
    private String uid;

    private String from;
    private String to;
    private String type;
    private String time;


    public GTFSTransfer(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        from          = getStringFieldOrNull(record, positions, GTFSTransferFields.FROM );
        to        = getStringFieldOrNull(record, positions,GTFSTransferFields.TO );
        type         = getStringFieldOrNull(record, positions,GTFSTransferFields.TYPE );
        time    = getStringFieldOrNull(record, positions,GTFSTransferFields.TIME );

    }

    public String from() { return from;}
    public String to() { return to;}
    public String time() { return time;}
    public String type() { return type;}


    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( from != null && from.length() > 0){
            sb.append("From: " ).append(from).append("   ");
        }

        if ( to != null && to.length() > 0){
            sb.append("To: " ).append(to).append("   ");
        }

        if ( time != null && time.length() > 0){
            sb.append("Time: " ).append(time).append("   ");
        }

        if ( type != null && type.length() > 0){
            sb.append("Type: " ).append(type).append("   ");
        }


        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerAgency();
    }
}