/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSCalendarFields;

import java.util.Map;

public class GTFSCalendar {
    private String uid;
    private String service;
    private String monday;
    private String tuesday;
    private String wednesday;
    private String thursday;
    private String friday;
    private String saturday;
    private String sunday;
    private String start;
    private String end;

    public GTFSCalendar(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();

        if (positions.containsKey(GTFSCalendarFields.SERVICE))
            service = record.get(positions.get(GTFSCalendarFields.SERVICE)).trim();

        if (positions.containsKey(GTFSCalendarFields.MONDAY))
            monday = record.get(GTFSCalendarFields.MONDAY).trim();

        if (positions.containsKey(GTFSCalendarFields.TUESDAY))
            tuesday = record.get(GTFSCalendarFields.TUESDAY).trim();

        if (positions.containsKey(GTFSCalendarFields.WEDNESDAY))
            wednesday = record.get(GTFSCalendarFields.WEDNESDAY).trim();

        if (positions.containsKey(GTFSCalendarFields.THURSDAY))
            thursday = record.get(GTFSCalendarFields.THURSDAY).trim();

        if (positions.containsKey(GTFSCalendarFields.FRIDAY))
            friday = record.get(GTFSCalendarFields.FRIDAY).trim();

        if (positions.containsKey(GTFSCalendarFields.SATURDAY))
            saturday = record.get(GTFSCalendarFields.SATURDAY).trim();

        if (positions.containsKey(GTFSCalendarFields.SUNDAY))
            sunday = record.get(GTFSCalendarFields.SUNDAY).trim();

        if (positions.containsKey(GTFSCalendarFields.START))
            start = record.get(GTFSCalendarFields.START).trim();

        if (positions.containsKey(GTFSCalendarFields.END))
            end = record.get(GTFSCalendarFields.END).trim();
    }

    public String service() { return service;}
    public String monday() { return monday;}
    public String tuesday() { return tuesday;}
    public String wednesday() { return wednesday;}
    public String thursday() { return thursday;}
    public String friday() { return friday;}
    public String saturday() { return saturday;}
    public String sunday() { return sunday;}
    public String start() { return start;}
    public String end() {return end; }
}
