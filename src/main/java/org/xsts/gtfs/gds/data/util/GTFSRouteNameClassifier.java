/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.util;

public class GTFSRouteNameClassifier {
    public static final int UNKNOWN = 0;
    public static final int NUMBER = 100;
    public static final int NUMBER_ALPHA = 200;
    public static final int ALPHA_NUMBER = 300;
    public static final int ALPHA = 400;



    public GTFSRouteNameClassifier() {

    }

    public int detectType(String name){
        int type = UNKNOWN;
        if (detectNumber(name) == true)
            type = NUMBER;
        else if (detectAlpha(name) == true)
            type = ALPHA;
        else if (detectNumberAlpha(name) == true)
            type = NUMBER_ALPHA;
        else if (detectAlphaNumber(name))
            type = ALPHA_NUMBER;

        return type;
    }

    public static boolean isDigit(char c){
        if (c < '0' ||c >'9')
            return false;
        return true;
    }


    public static boolean isAlpha(char c) {
        if (isDigit(c) == false)
            return true;
        return false;
    }

    boolean detectNumber(String name){
        boolean result = true;
        char[] glyphs = name.toCharArray();
        for (char c : glyphs){
            if (isDigit(c) == false){
                result = false;
                break;
            }
        }
        return result;
    }


    boolean detectAlpha(String name){
        boolean result = true;
        char[] glyphs = name.toCharArray();
        for (char c : glyphs){
            if (isAlpha(c) == false){
                result = false;
                break;
            }
        }
        return result;
    }


    boolean detectNumberAlpha(String name){
        boolean result = true;
        boolean checkAlpha = false;
        char[] glyphs = name.toCharArray();
        if (isAlpha(glyphs[0])) {
            result = false;
        } else {
            for (char c : glyphs){
                if (checkAlpha == false){
                    if (isDigit(c) == false){
                        checkAlpha = true;
                    }
                } else {
                    if (isAlpha(c) == false){
                        result = false;
                    }
                }

            }
            if (checkAlpha == false)
                result = false;
        }

        return result;
    }

    boolean detectAlphaNumber(String name){
        boolean result = true;
        boolean checkDigit = false;
        char[] glyphs = name.toCharArray();
        if (isDigit(glyphs[0])) {
            result = false;
        } else {
            for (char c : glyphs){
                if (checkDigit == false){
                    if (isAlpha(c) == false){
                        checkDigit = true;
                    }
                } else {
                    if (isDigit(c) == false){
                        result = false;
                    }
                }

            }
            if (checkDigit == false)
                result = false;
        }

        return result;
    }
}
