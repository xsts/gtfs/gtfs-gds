/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSStopFields {
    public static final String STOP_ID = "stop_id";
    public static final String STOP_CODE = "stop_code";
    public static final String STOP_NAME = "stop_name";
    public static final String STOP_DESC = "stop_desc";
    public static final String STOP_LAT = "stop_lat";
    public static final String STOP_LON = "stop_lon";
    public static final String ZONE_ID = "zone_id";
    public static final String STOP_URL = "stop_url";
    public static final String LOCATION_TYPE = "location_type";
    public static final String PARENT_STATION = "parent_station";
    public static final String STOP_TIMEZONE = "stop_timezone";
    public static final String WHEELCHAIR_BOARDING = "wheelchair_boarding";
    public static final String LEVEL_ID = "level_id";
    public static final String PLATFORM_CODE = "platform_code";


    public static final String ID = STOP_ID;
    public static final String CODE = STOP_CODE;
    public static final String NAME = STOP_NAME;
    public static final String DESC = STOP_DESC;
    public static final String LAT = STOP_LAT;
    public static final String LON = STOP_LON;
    public static final String ZONE = ZONE_ID;
    public static final String URL = STOP_URL;
    public static final String LOCATION = LOCATION_TYPE;
    public static final String PARENT= PARENT_STATION;
    public static final String TIMEZONE = STOP_TIMEZONE;
    public static final String WHEELCHAIR = WHEELCHAIR_BOARDING;
    public static final String LEVEL = LEVEL_ID;
    public static final String PLATFORM = PLATFORM_CODE;

    public static final String DESCRIPTION = DESC;
    public static final String LATITUDE = LAT;
    public static final String LONGITUDE = LON;
    public static final String TZ = TIMEZONE;

}
