/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSWheelchairSupport {
    public static final Integer INFORMATION_UNAVAILABLE = 0;
    public static final Integer ALLOW_ONE = 0;
    public static final Integer NOT_SUPPORTING = 0;

}
