/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.timesheet;

import org.xsts.gtfs.gds.data.collections.GTFSRouteList;
import org.xsts.gtfs.gds.data.collections.GTFSStopList;
import org.xsts.gtfs.gds.data.collections.GTFSStopTimeList;
import org.xsts.gtfs.gds.data.collections.GTFSStopTimeListMap;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.types.GTFSRoute;

import java.util.HashMap;
import java.util.Map;

public class TOQuickTimeBuilder {
    public static Map<String, Map<String,TOStationList>> generate(GTFSRouteList routes, GTFSStopList stops) {
        Map<String,Map<String,TOStationList>> network = new HashMap<>();
        GTFSCache bigCache = GTFSCache.fromGlobal();
        for ( String routeID: routes.keySet()) {
            GTFSRoute route =  routes.get(routeID);
            GTFSStopTimeList stopTimeList = GTFSStopTimeListMap.getLongestListFromGlobal(routeID);
            if ( stopTimeList == null ||stopTimeList.getStopTimes() == null || stopTimeList.getStopTimes().size() < 2)
                continue;
            TOStationListBuilder stationListBuilder = new TOStationListBuilder();
            TOStationList stationList = stationListBuilder.build(stops,stopTimeList );
            if (stationList == null)
                continue;
            stationList.sort(TOStationList.SortMode.TIME_OFFSET);
            if ( network.containsKey(route.shortName()) == false) {
                Map<String, TOStationList> miniMap = new HashMap<>();
                network.put(route.shortName(), miniMap);
            }
            network.get(route.shortName()).put(route.id(),stationList);
        }


        return network;
    }


    public static Map<String, Map<String,TOStationList>> generate(GTFSRouteList routes, GTFSStopList stops, TOStopTool tool) {
        Map<String,Map<String,TOStationList>> network = new HashMap<>();
        GTFSCache bigCache = GTFSCache.fromGlobal();
        for ( String routeID: routes.keySet()) {
            GTFSRoute route =  routes.get(routeID);
            GTFSStopTimeList stopTimeList = GTFSStopTimeListMap.getLongestListFromGlobal(routeID);
            if ( stopTimeList == null ||stopTimeList.getStopTimes() == null || stopTimeList.getStopTimes().size() < 2)
                continue;
            TOStationListBuilder stationListBuilder = new TOStationListBuilder();
            TOStationList stationList = stationListBuilder.build(stops,stopTimeList, tool );
            if (stationList == null)
                continue;
            stationList.sort(TOStationList.SortMode.TIME_OFFSET);
            if ( network.containsKey(route.shortName()) == false) {
                Map<String, TOStationList> miniMap = new HashMap<>();
                network.put(route.shortName(), miniMap);
            }
            network.get(route.shortName()).put(route.id(),stationList);
        }


        return network;
    }
}
