/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSBidirectionalType {
    public static final Integer UNIDIRECTIONAL = 0;
    public static final Integer BIDIRECTIONAL = 1;

    public static final Integer ONE_WAY = UNIDIRECTIONAL;
}
