/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.timesheet;

import java.util.ArrayList;
import java.util.List;

public class TOStationAliases {
    private List<TOStation> aliases;
    private TOStation centerpoint = null;

    public TOStationAliases() {
        aliases = new ArrayList<>();
    }


    private void updateCenterpoint(){
        Long cLat = 0L;
        Long cLon = 0L;
        for (TOStation station: aliases){
            cLat += station.latitude();
            cLon += station.longitude();
        }
        cLat /= aliases.size();
        cLon /= aliases.size();

        centerpoint = new TOStation(aliases.get(0).name(),0,cLat,cLon);
    }

    public void addStation(TOStation station) {
        aliases.add(station);
        updateCenterpoint();
    }

    public TOStation getCenterpoint() { return centerpoint;}
}
