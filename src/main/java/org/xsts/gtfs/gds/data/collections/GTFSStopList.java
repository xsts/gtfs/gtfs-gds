/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gds.data.types.GTFSStop;

import java.util.HashMap;
import java.util.Map;

public class GTFSStopList implements KeyValueList {
    private Map<String, GTFSStop> stops;

    public GTFSStopList(){
        stops = new HashMap<>();
    }

    public void add(String id, GTFSStop stop){
        stops.put(id,stop);
    }
    @Override
    public void add(String id, Object object) {
        add(id, (GTFSStop) object);
    }

    public GTFSStop get(String id) {
        return stops.get(id);
    }

    public boolean isEmpty() { return stops.size() == 0;}
    public Map<String, GTFSStop> getStops() { return stops; }
    public int count() { return stops.size();}

}
