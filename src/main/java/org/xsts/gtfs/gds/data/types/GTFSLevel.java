/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSLevelFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSLevel extends GTFSInfo{

    private String uid;

    private String id;
    private String name;
    private String index;


    public GTFSLevel(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        id          = getStringFieldOrNull(record, positions, GTFSLevelFields.ID );
        name        = getStringFieldOrNull(record, positions,GTFSLevelFields.NAME );
        index         = getStringFieldOrNull(record, positions,GTFSLevelFields.INDEX );
    }

    public String id() { return id;}
    public String name() { return name;}
    public String index() { return index;}


    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( id != null && id.length() > 0){
            sb.append("ID: " ).append(id).append("   ");
        }

        if ( name != null && name.length() > 0){
            sb.append("Name: " ).append(name).append("   ");
        }

        if ( index != null && index.length() > 0){
            sb.append("Index: " ).append(index).append("   ");
        }



        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerAgency();
    }
}
