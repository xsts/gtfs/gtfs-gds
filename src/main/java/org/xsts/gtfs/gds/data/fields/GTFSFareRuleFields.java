/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSFareRuleFields {
    public static final String FARE_ID = "fare_id";
    public static final String ROUTE_ID = "route_id";
    public static final String ORIGIN_ID = "origin_id";
    public static final String DESTINATION_ID = "destination_id";
    public static final String CONTAINS_ID = "contains_id";

    public static final String FARE = FARE_ID;
    public static final String ROUTE = ROUTE_ID;
    public static final String ORIGIN = ORIGIN_ID;
    public static final String DESTINATION = DESTINATION_ID;
    public static final String CONTAINS = CONTAINS_ID;
}
