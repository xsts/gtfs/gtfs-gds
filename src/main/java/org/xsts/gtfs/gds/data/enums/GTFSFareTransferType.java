/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSFareTransferType {
    public static final Integer NOT_ALLOWED = 0;
    public static final Integer ONCE = 1;
    public static final Integer TWICE = 2;
    public static final Integer UNLIMITED = null;
}
