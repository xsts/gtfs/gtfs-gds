/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSRouteType {
    public static final Integer LIGHT_RAIL = 0;
    public static final Integer TRAM = LIGHT_RAIL;
    public static final Integer STREETCAR = LIGHT_RAIL;
    public static final Integer TRAMWAY = TRAM;

    public static final Integer SUBWAY = 1;
    public static final Integer METRO = SUBWAY;
    public static final Integer UNDERGROUND = SUBWAY;

    public static final Integer RAIL = 2;
    public static final Integer BUS = 3;
    public static final Integer FERRY = 4;
    public static final Integer CABLE_TRAM = 5;

    public static final Integer AERIAL_LIFT = 6;
    public static final Integer SUSPENDED_CABLE_CAR = AERIAL_LIFT;
    public static final Integer GONDOLA = AERIAL_LIFT;
    public static final Integer AERIAL_TRAMWAY = AERIAL_LIFT;
    public static final Integer CHAIR_LIFT = AERIAL_LIFT;

    public static final Integer FUNICULAR = 7;
    public static final Integer TROLLEYBUS = 8;
    public static final Integer MONORAIL = 9;


    public static String valueToString(String value){
        if (value == null)
            return null;

        Integer intValue = Integer.parseInt(value);
        return valueToString(intValue);
    }

    public static String valueToString(Integer value) {
        if(value == LIGHT_RAIL)
            return TEXT_LIGHT_RAIL;

        if(value == SUBWAY)
            return TEXT_SUBWAY;

        if(value == RAIL)
            return TEXT_RAIL;

        if(value == BUS)
            return TEXT_BUS;

        if(value == FERRY)
            return TEXT_FERRY;

        if(value == CABLE_TRAM)
            return TEXT_CABLE_TRAM;

        if(value == AERIAL_LIFT)
            return TEXT_AERIAL_LIFT;

        if(value == FUNICULAR)
            return TEXT_FUNICULAR;

        if(value == TROLLEYBUS)
            return TEXT_TROLLEYBUS;

        if(value == MONORAIL)
            return TEXT_MONORAIL;

        return GTFSExtendedRouteTypes.instance().valueToString(value);
    }

    public static final String TEXT_LIGHT_RAIL = "Light Rail";
    public static final String TEXT_SUBWAY = "Subway";
    public static final String TEXT_RAIL = "Rail";
    public static final String TEXT_BUS = "Bus";
    public static final String TEXT_FERRY = "Ferry";
    public static final String TEXT_CABLE_TRAM = "Cable Tram";
    public static final String TEXT_AERIAL_LIFT = "Aerial lift";
    public static final String TEXT_FUNICULAR = "Funicular";
    public static final String TEXT_TROLLEYBUS = "Trolleybus";
    public static final String TEXT_MONORAIL = "Monorail";

}

