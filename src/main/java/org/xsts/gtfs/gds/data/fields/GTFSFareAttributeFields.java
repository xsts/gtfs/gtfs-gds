/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSFareAttributeFields {
    public static final String FARE_ID = "fare_id";
    public static final String PRICE = "price";
    public static final String CURRENCY_TYPE = "currency_type";
    public static final String PAYMENT_METHOD = "payment_method";
    public static final String TRANSFERS = "transfers";
    public static final String AGENCY_ID = "agency_id";
    public static final String TRANSFER_DURATION = "transfer_duration";

    public static final String ID = FARE_ID;
    public static final String CURRENCY = CURRENCY_TYPE;
    public static final String METHOD = PAYMENT_METHOD;
    public static final String AGENCY = AGENCY_ID;
    public static final String TRANSFER = TRANSFER_DURATION;

}

