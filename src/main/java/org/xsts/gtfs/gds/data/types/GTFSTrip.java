/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSTripFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSTrip {
    private String uid;


    private String route;
    private String service;
    private String id;
    private String headsign;
    private String shortName;
    private String direction;
    private String block;
    private String shape;
    private String wheelchair;
    private String bikes;

    public GTFSTrip(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        if (positions.containsKey(GTFSTripFields.ROUTE))
            route = record.get(GTFSTripFields.ROUTE).trim();

        if (positions.containsKey(GTFSTripFields.SERVICE))
            service = record.get(GTFSTripFields.SERVICE).trim();

        if (positions.containsKey(GTFSTripFields.ID))
            id = record.get(GTFSTripFields.ID).trim();

        if (positions.containsKey(GTFSTripFields.ROUTE))
            route = record.get(GTFSTripFields.ROUTE).trim();

        if (positions.containsKey(GTFSTripFields.HEADSIGN))
            headsign = record.get(GTFSTripFields.HEADSIGN).trim();

        if (positions.containsKey(GTFSTripFields.SHORT_NAME))
            shortName = record.get(GTFSTripFields.SHORT_NAME).trim();

        if (positions.containsKey(GTFSTripFields.DIRECTION))
            direction = record.get(GTFSTripFields.DIRECTION).trim();

        if (positions.containsKey(GTFSTripFields.BLOCK))
            block = record.get(GTFSTripFields.BLOCK).trim();

        if (positions.containsKey(GTFSTripFields.SHAPE))
            shape = record.get(GTFSTripFields.SHAPE).trim();

        if (positions.containsKey(GTFSTripFields.WHEELCHAIR))
            wheelchair = record.get(GTFSTripFields.WHEELCHAIR).trim();

        if (positions.containsKey(GTFSTripFields.BIKES))
            bikes = record.get(GTFSTripFields.BIKES).trim();

    }

    public String route() { return route;}
    public String service() { return service;}
    public String id() { return id;}
    public String headsign() { return headsign;}
    public String shortName() { return shortName;}
    public String direction() { return direction;}
    public String block() { return block;}
    public String shape() { return shape;}
    public String wheelchair() { return wheelchair;}
    public String bikes() { return bikes;}


    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( route != null && route.length() > 0){
            sb.append("Route ID: " ).append(route).append("   ");
        }

        if ( service != null && service.length() > 0){
            sb.append("Service ID: " ).append(service).append("   ");
        }

        if ( id != null && id.length() > 0){
            sb.append("Trip ID: " ).append(id).append("   ");
        }

        if ( headsign != null && headsign.length() > 0){
            sb.append("Head Sign: " ).append(headsign).append("   ");
        }

        if ( shortName != null && shortName.length() > 0){
            sb.append("Short Name: " ).append(shortName).append("   ");
        }

        if ( direction != null && direction.length() > 0){
            sb.append("Direction: " ).append(direction).append("   ");
        }

        if ( block != null && block.length() > 0){
            sb.append("Block: " ).append(block).append("   ");
        }

        if ( shape != null && shape.length() > 0){
            sb.append("Shape: " ).append(shape).append("   ");
        }

        if ( wheelchair != null && wheelchair.length() > 0){
            sb.append("Wheelchair: " ).append(wheelchair).append("   ");
        }

        if ( bikes != null && bikes.length() > 0){
            sb.append("Bikes: " ).append(bikes).append("   ");
        }

        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerTrip();
    }
}


