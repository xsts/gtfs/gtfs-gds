/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.timesheet;

import org.xsts.core.data.util.LatLongConvertor;
import org.xsts.gtfs.gds.data.collections.GTFSStopList;
import org.xsts.gtfs.gds.data.types.GTFSStop;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class TOStationRouteAliases {
    private Map<String, TOStationAliases> aliases = null;

    public TOStationRouteAliases() {
        aliases = new HashMap<>();
    }

    public TOStationRouteAliases(GTFSStopList stopList) {
        aliases = new HashMap<>();
        Map<String, GTFSStop> stops = stopList.getStops();
        for ( String key: stops.keySet()){
            GTFSStop stop = stops.get(key);
            String[] goodNames = stop.name().split(Pattern.quote("("));

            long intLat = LatLongConvertor.convertSDL(stop.latitude());
            long intLon = LatLongConvertor.convertSDL(stop.longitude());

            TOStation station = new TOStation(goodNames[0],0,intLat,intLon);
            addStation(station);
        }
    }

    public void addStation(TOStation station){
        String name = station.name();
        if (aliases.containsKey(name) == false)
            aliases.put(name, new TOStationAliases());
        aliases.get(name).addStation(station);

    }

    public TOStation getAliasStation(TOStation station) {
        return aliases.get(station.name()).getCenterpoint();
    }

    public Map<String, TOStationAliases> getAliases() { return aliases;}
    public boolean hasStationName(String name) { return aliases.containsKey(name);}
}
