/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSShapeFields {
    public static final String SHAPE_ID = "shape_id";
    public static final String SHAPE_PT_LAT = "shape_pt_lat";
    public static final String SHAPE_PT_LON = "shape_pt_lon";
    public static final String SHAPE_PT_SEQUENCE = "shape_pt_sequence";
    public static final String SHAPE_DIST_TRAVELED = "shape_dist_traveled";

    public static final String ID = SHAPE_ID;
    public static final String PT_LAT = SHAPE_PT_LAT;
    public static final String PT_LON = SHAPE_PT_LON;
    public static final String PT_SEQUENCE = SHAPE_PT_SEQUENCE;
    public static final String DIST_TRAVELED = SHAPE_DIST_TRAVELED;

    public static final String LAT = PT_LAT;
    public static final String LON = PT_LON;
    public static final String SEQUENCE = PT_SEQUENCE;
    public static final String TRAVELED = DIST_TRAVELED;

}

