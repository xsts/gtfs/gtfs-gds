/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.LinearValueList;
import org.xsts.gtfs.gds.data.types.GTFSCalendar;

import java.util.ArrayList;
import java.util.List;

public class GTFSCalendarLinearList implements LinearValueList {
    List<GTFSCalendar> calendars;

    public GTFSCalendarLinearList() {
        calendars = new ArrayList<>();
    }
    public  void add(GTFSCalendar calendar){
        calendars.add(calendar);
    }

    public List<GTFSCalendar> getAll() { return calendars; }

    public GTFSCalendar get(int pos) {
        return calendars.get(pos);
    }

    @Override
    public void add(Object object) {
        calendars.add((GTFSCalendar) object);
    }

    @Override
    public Object getAt(int pos) {
        return calendars.get(pos);
    }

    @Override
    public boolean isEmpty() {
        return calendars.size() == 0;
    }
}