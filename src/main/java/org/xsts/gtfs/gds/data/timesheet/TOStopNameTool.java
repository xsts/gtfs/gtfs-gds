/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.timesheet;

import org.xsts.gtfs.gds.data.types.GTFSStop;

public class TOStopNameTool implements TOStopTool  {
    @Override
    public String getName(GTFSStop stop) {
        return stop.name();
    }
}
