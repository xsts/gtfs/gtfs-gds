/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSServiceExceptionType {
    public static final Integer SERVICE_ADDED = 1;
    public static final Integer SERVICE_REMOVED = 2;

    public static final Integer ADDED = SERVICE_ADDED;
    public static final Integer REMOVED = SERVICE_REMOVED;
}
