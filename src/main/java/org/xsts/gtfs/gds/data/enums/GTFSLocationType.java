/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSLocationType {
    public static final Integer STOP_OR_PLATFORM = 0;

    public static final Integer STOP = STOP_OR_PLATFORM;
    public static final Integer PLATFORM = STOP_OR_PLATFORM;

    public static final Integer STATION = 1;

    public static final Integer ENTRANCE_OR_EXIT = 2;

    public static final Integer ENTRANCE = ENTRANCE_OR_EXIT;
    public static final Integer EXIT = ENTRANCE_OR_EXIT;

    public static final Integer GENERIC_NODE = 3;

    public static final Integer BOARDING_AREA = 4;

}
