/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSFeedInfoFields {
    public static final String FEED_PUBLISHER_NAME = "feed_publisher_name";
    public static final String FEED_PUBLISHER_URL = "feed_publisher_url";
    public static final String FEED_LANG = "feed_lang";
    public static final String DEFAULT_LANG = "default_lang";
    public static final String FEED_START_DATE = "feed_start_date";
    public static final String FEED_END_DATE = "feed_end_date";
    public static final String FEED_VERSION = "feed_version";
    public static final String FEED_CONTACT_EMAIL = "feed_contact_email";
    public static final String FEED_CONTACT_URL = "feed_contact_url";

    public static final String PUBLISHER_NAME = FEED_PUBLISHER_NAME;
    public static final String PUBLISHER_URL = FEED_PUBLISHER_URL;
    public static final String LANG = FEED_LANG;
    public static final String START_DATE = FEED_START_DATE;
    public static final String END_DATE = FEED_END_DATE;
    public static final String VERSION = FEED_VERSION;
    public static final String CONTACT_EMAIL = FEED_CONTACT_EMAIL;
    public static final String CONTACT_URL = FEED_CONTACT_URL;

    public static final String PUBLISHER = PUBLISHER_NAME;
    public static final String CONTACT = CONTACT_EMAIL;
    public static final String START = START_DATE;
    public static final String END = END_DATE;
    public static final String EMAIL = CONTACT_EMAIL;

}

