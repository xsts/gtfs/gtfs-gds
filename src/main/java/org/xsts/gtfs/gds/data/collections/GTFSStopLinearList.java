/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.LinearValueList;
import org.xsts.gtfs.gds.data.types.GTFSStop;

import java.util.ArrayList;
import java.util.List;

public class GTFSStopLinearList implements LinearValueList {
    List<GTFSStop> stops;

    public GTFSStopLinearList() {
        stops = new ArrayList<>();
    }
    public  void add(GTFSStop stop){
        stops.add(stop);
    }

    public List<GTFSStop> getAll() { return stops; }


    @Override
    public void add(Object object) {
        stops.add((GTFSStop) object);
    }

    @Override
    public Object getAt(int pos) {
        return stops.get(pos);
    }

    @Override
    public boolean isEmpty() {
        return stops.size() == 0;
    }


}
