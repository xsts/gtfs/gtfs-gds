/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.timesheet;

import org.xsts.core.data.util.LatLongConvertor;
import org.xsts.gtfs.gds.data.collections.GTFSStopList;
import org.xsts.gtfs.gds.data.collections.GTFSStopTimeList;
import org.xsts.gtfs.gds.data.types.GTFSStop;
import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.regex.Pattern;

import static java.lang.System.exit;

public class TOStationListBuilder {
    public  int getAbsoluteMinute( GTFSStopTime stopTime){
        int absMinute = 0;
        try {
            String departure = stopTime.departure();
            String arrival = stopTime.arrival();

            if (departure.isEmpty() && arrival.isEmpty()) {
                return 0;
            }

            if (departure.isEmpty() && !arrival.isEmpty()) {
                departure = arrival;
            }

            String[] tokens = departure.split(":");
            Integer hour = Integer.parseInt(tokens[0]);
            Integer minute = Integer.parseInt(tokens[1]);
            absMinute = hour * 60 + minute;
        } catch(NumberFormatException e) {
            e.printStackTrace();
            exit(1);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return absMinute;
    }

    public  int deltaMinute(GTFSStopTime first, GTFSStopTime  second) {
        return getAbsoluteMinute(second) - getAbsoluteMinute(first) ;
    }

    public  TOStationList build(GTFSStopList stopList, GTFSStopTimeList stopTimeList) {
        TOStationList list = new TOStationList();
        GTFSStopTime stopTimeReference = null;
        try {
            // I assumed the first one was 1. Wrong. 20200226 Feb 26, 2020
            for (  int firstSequence = 0; firstSequence < 1000; firstSequence++){
                Integer seq = firstSequence;

                GTFSStopTime reference = stopTimeList.get(seq.toString());
                if (reference != null){
                    stopTimeReference = reference;
                    break;
                }
            }

            // I'd rather prefer to lose a route that to lose everything
            if (stopTimeReference == null)
                return null;

            int referenceMinute = getAbsoluteMinute(stopTimeReference);

            for ( String seq: stopTimeList.getStopTimes().keySet()) {
                GTFSStopTime stopTime = stopTimeList.get(seq);
                int absoluteMinute = getAbsoluteMinute(stopTime);
                int delta = absoluteMinute - referenceMinute;
                GTFSStop stop = stopList.get(stopTime.stop());
                String[] goodNames = stop.name().split(Pattern.quote("("));

                long intLat = LatLongConvertor.convertSDL(stop.latitude());
                long intLon = LatLongConvertor.convertSDL(stop.longitude());



                list.add(new TOStation(goodNames[0],delta,intLat,intLon));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }



        return list;
    }




    public  TOStationList build(GTFSStopList stopList, GTFSStopTimeList stopTimeList, TOStopTool tool) {
        TOStationList list = new TOStationList();
        GTFSStopTime stopTimeReference = null;
        try {
            // I assumed the first one was 1. Wrong. 20200226 Feb 26, 2020
            for (  int firstSequence = 0; firstSequence < 1000; firstSequence++){
                Integer seq = firstSequence;

                GTFSStopTime reference = stopTimeList.get(seq.toString());
                if (reference != null){
                    stopTimeReference = reference;
                    break;
                }
            }

            // I'd rather prefer to lose a route that to lose everything
            if (stopTimeReference == null)
                return null;

            int referenceMinute = getAbsoluteMinute(stopTimeReference);

            for ( String seq: stopTimeList.getStopTimes().keySet()) {
                GTFSStopTime stopTime = stopTimeList.get(seq);
                int absoluteMinute = getAbsoluteMinute(stopTime);
                int delta = absoluteMinute - referenceMinute;
                GTFSStop stop = stopList.get(stopTime.stop());
                //String[] goodNames = stop.name().split(Pattern.quote("("));

                String[] goodNames = tool.getName(stop).split(Pattern.quote("("));

                long intLat = LatLongConvertor.convertSDL(stop.latitude());
                long intLon = LatLongConvertor.convertSDL(stop.longitude());



                list.add(new TOStation(goodNames[0],delta,intLat,intLon));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }



        return list;
    }
}
