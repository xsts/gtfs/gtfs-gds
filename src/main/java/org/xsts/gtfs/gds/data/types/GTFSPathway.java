/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSPathwayFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSPathway extends GTFSInfo{
    private String uid;

    private String id;
    private String from;
    private String to;
    private String mode;
    private String bidirectional;
    private String length;
    private String time;
    private String slope;
    private String width;
    private String signposted;
    private String reverseSignposted;

    public GTFSPathway(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        id                  = getStringFieldOrNull(record, positions, GTFSPathwayFields.ID );
        from                = getStringFieldOrNull(record, positions,GTFSPathwayFields.FROM );
        to                  = getStringFieldOrNull(record, positions,GTFSPathwayFields.TO );
        mode                = getStringFieldOrNull(record, positions,GTFSPathwayFields.MODE );
        bidirectional       = getStringFieldOrNull(record, positions,GTFSPathwayFields.BIDIRECTIONAL );
        length              = getStringFieldOrNull(record, positions,GTFSPathwayFields.LENGTH );
        time                = getStringFieldOrNull(record, positions,GTFSPathwayFields.TIME );
        slope               = getStringFieldOrNull(record, positions,GTFSPathwayFields.SLOPE );
        width               = getStringFieldOrNull(record, positions,GTFSPathwayFields.WIDTH );
        signposted          = getStringFieldOrNull(record, positions,GTFSPathwayFields.SIGNPOSTED);
        reverseSignposted   = getStringFieldOrNull(record, positions,GTFSPathwayFields.REVERSED_SIGNPOSTED );
    }

    public String id() { return id;}
    public String from() { return from;}
    public String to() { return to;}
    public String mode() { return mode;}
    public String length() { return length;}
    public String time() { return time;}
    public String slope() { return slope;}
    public String width() { return width;}
    public String signposted() { return signposted;}
    public String reverseSignposted() { return reverseSignposted;}

    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( id != null && id.length() > 0){
            sb.append("ID: " ).append(id).append("   ");
        }

        if ( from != null && from.length() > 0){
            sb.append("From: " ).append(from).append("   ");
        }

        if ( to != null && to.length() > 0){
            sb.append("To: " ).append(to).append("   ");
        }

        if ( mode != null && mode.length() > 0){
            sb.append("Mode: " ).append(mode).append("   ");
        }

        if ( length != null && length.length() > 0){
            sb.append("Length: " ).append(length).append("   ");
        }

        if ( time != null && time.length() > 0){
            sb.append("Time: " ).append(time).append("   ");
        }

        if ( slope != null && slope.length() > 0){
            sb.append("Slope: " ).append(slope).append("   ");
        }

        if ( width != null && width.length() > 0){
            sb.append("Width: " ).append(width).append("   ");
        }

        if ( signposted != null && signposted.length() > 0){
            sb.append("Signposted: " ).append(signposted).append("   ");
        }

        if ( reverseSignposted != null && reverseSignposted.length() > 0){
            sb.append("Reverse signposted: " ).append(reverseSignposted).append("   ");
        }


        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerAgency();
    }
}
