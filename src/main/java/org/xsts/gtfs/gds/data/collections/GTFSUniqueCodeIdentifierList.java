/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gds.data.types.GTFSUniqueCodeIdentifier;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GTFSUniqueCodeIdentifierList implements KeyValueList {
    private Map<String, GTFSUniqueCodeIdentifier> uids;

    public GTFSUniqueCodeIdentifierList(){
        uids = new HashMap<>();
    }

    @Override
    public void add(String id, Object object) {
        add(id, (GTFSUniqueCodeIdentifier) object);
    }

    public void add(String id, GTFSUniqueCodeIdentifier uid){
        uids.put(id, uid);
    }
    public GTFSUniqueCodeIdentifier get(String id) {
        return uids.get(id);
    }
    public boolean isEmpty() { return uids.size() == 0;}
    public GTFSUniqueCodeIdentifier first() {
        return uids.get(uids.keySet().toArray()[0]);
    }
    public Set<String> keySet() { return uids.keySet(); }

}
