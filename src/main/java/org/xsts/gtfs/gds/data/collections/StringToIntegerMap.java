/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class StringToIntegerMap implements KeyValueList {
    private Map<String, Integer> theMap = null;
    public StringToIntegerMap() {
        theMap = new HashMap<>();
    }

    @Override
    public void add(String id, Object object) {
        add(id, (Integer) object);
    }
    public void add(String id, Integer GTFSAgency){
        theMap.put(id, GTFSAgency);
    }
    public Integer get(String code) {
        return theMap.get(code);
    }
    @Override
    public boolean isEmpty() { return theMap.size() == 0;}

    public Integer getFirstAgency() {
        return theMap.get(theMap.keySet().toArray()[0]);
    }
    public Set<String> keySet() { return theMap.keySet(); }
}
