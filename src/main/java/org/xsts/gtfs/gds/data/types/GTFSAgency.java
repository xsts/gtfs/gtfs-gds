/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSAgencyFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSAgency extends GTFSInfo{
    private String uid;

    private String id;
    private String name;
    private String url;
    private String timezone;
    private String lang;
    private String phone;
    private String fareUrl;
    private String email;

    public GTFSAgency(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        id          = getStringFieldOrNull(record, positions, GTFSAgencyFields.ID );
        name        = getStringFieldOrNull(record, positions,GTFSAgencyFields.NAME );
        url         = getStringFieldOrNull(record, positions,GTFSAgencyFields.URL );
        timezone    = getStringFieldOrNull(record, positions,GTFSAgencyFields.TIMEZONE );
        lang        = getStringFieldOrNull(record, positions,GTFSAgencyFields.LANG );
        phone       = getStringFieldOrNull(record, positions,GTFSAgencyFields.PHONE );
        fareUrl     = getStringFieldOrNull(record, positions,GTFSAgencyFields.FARE_URL );
        email       = getStringFieldOrNull(record, positions,GTFSAgencyFields.EMAIL );
    }

    public String id() { return id;}
    public String name() { return name;}
    public String url() { return url;}
    public String timezone() { return timezone;}
    public String lang() { return lang;}
    public String phone() { return phone;}
    public String fareUrl() { return fareUrl;}
    public String email() { return email;}

    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( id != null && id.length() > 0){
            sb.append("ID: " ).append(id).append("   ");
        }

        if ( name != null && name.length() > 0){
            sb.append("Name: " ).append(name).append("   ");
        }

        if ( url != null && url.length() > 0){
            sb.append("URL: " ).append(url).append("   ");
        }

        if ( timezone != null && timezone.length() > 0){
            sb.append("Time zone: " ).append(timezone).append("   ");
        }

        if ( lang != null && lang.length() > 0){
            sb.append("Lang: " ).append(lang).append("   ");
        }

        if ( phone != null && phone.length() > 0){
            sb.append("Phone: " ).append(phone).append("   ");
        }

        if ( fareUrl != null && fareUrl.length() > 0){
            sb.append("Fare URL: " ).append(fareUrl).append("   ");
        }

        if ( email != null && email.length() > 0){
            sb.append("Email: " ).append(email).append("   ");
        }

        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerAgency();
    }
}
