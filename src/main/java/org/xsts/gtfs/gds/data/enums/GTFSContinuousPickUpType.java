/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSContinuousPickUpType {
    public static final Integer CONTINUOUS = 0;
    public static final Integer UNAVAILABLE = 1;
    public static final Integer CONTACT_AGENCY = 2;
    public static final Integer CONTACT_DRIVER = 3;
}
