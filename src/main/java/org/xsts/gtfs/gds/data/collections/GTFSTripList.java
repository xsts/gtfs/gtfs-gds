/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.TripleKeyValueList;
import org.xsts.gtfs.gds.data.types.GTFSTrip;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GTFSTripList implements TripleKeyValueList {

    private Map<String, Map<String, Map<String, GTFSTrip>>> trips = new HashMap<>();


    @Override
    public void add(String key1, String key2, String key3, Object object) {
        if ( get(key1) == null){
            Map<String, Map<String, GTFSTrip>> newMap = new HashMap<>();
            trips.put(key1, newMap);
        }

        if ( get(key1,key2) == null){
            Map<String, GTFSTrip> newMap = new HashMap<>();
            trips.get(key1).put(key2, newMap);
        }

        trips.get(key1).get(key2).put(key3, (GTFSTrip)object);
    }

    @Override
    public Object get(String key1) {
        return trips.get(key1);
    }



    @Override
    public Object get(String key1, String key2) {
        if ( trips.get(key1) == null)
            return null;

        return trips.get(key1).get(key2);
    }

    @Override
    public Object get(Object key1, String key2, String key3) {
        if ( trips.get(key1) == null)
            return null;
        if ( trips.get(key1).get(key2) == null)
            return null;

        return trips.get(key1).get(key2).get(key3);
    }

    public GTFSTrip getTrip(String routeID, String serviceID, String tripID) {
        return (GTFSTrip) get(routeID, serviceID, tripID);
    }

    @Override
    public boolean isEmpty() {
        int size = 0;
        for ( String key1 : trips.keySet()){
            for ( String key2: trips.get(key1).keySet()){
                for ( String  key3: trips.get(key1).get(key2).keySet()){
                    size ++;
                }
            }
        }
        if ( size > 0)
            return false;
        return true;
    }

    public Set<String> keySet() { return trips.keySet(); }
    public Set<String> keySet2(String key1) { return trips.get(key1).keySet(); }
    public Set<String> keySet3(String key1, String key2) { return trips.get(key1).get(key2).keySet(); }

    public Set<String> tripIDList() {
        Set<String> theTrips = new HashSet<>();
        for ( String key1: trips.keySet()){
            for ( String key2: keySet2(key1)) {
                for ( String key3 : keySet3(key1, key2)){
                    theTrips.add(key3);
                }
            }
        }
        return theTrips;
    }

    public Set<String> serviceIDList() {
        Set<String> theService = new HashSet<>();
        for ( String key1: trips.keySet()){
            for ( String key2: keySet2(key1)) {
                theService.add(key2);
            }
        }
        return theService;
    }

}