/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSServiceAvailability {
    public static final Integer UNAVAILABLE = 0;
    public static final Integer AVAILABLE = 1;

}
