/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;

import java.util.HashMap;
import java.util.Map;

public class StringToObjectMap implements KeyValueList {
    private Map<String, Object> theMap = null;
    public StringToObjectMap() {
        theMap = new HashMap<>();
    }

    @Override
    public void add(String id, Object object) {
        theMap.put(id, object);
    }
    @Override
    public Object get(String code) {
        return theMap.get(code);
    }
    @Override
    public boolean isEmpty() { return theMap.size() == 0;}
}
