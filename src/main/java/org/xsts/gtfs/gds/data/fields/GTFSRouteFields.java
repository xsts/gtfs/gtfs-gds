/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSRouteFields {
    public static final String ROUTE_ID = "route_id";
    public static final String AGENCY_ID = "agency_id";
    public static final String ROUTE_SHORT_NAME = "route_short_name";
    public static final String ROUTE_LONG_NAME = "route_long_name";
    public static final String ROUTE_DESC = "route_desc";
    public static final String ROUTE_TYPE = "route_type";
    public static final String ROUTE_URL = "route_url";
    public static final String ROUTE_COLOR = "route_color";
    public static final String ROUTE_TEXT_COLOR = "route_text_color";
    public static final String ROUTE_SORT_ORDER = "route_sort_order";
    public static final String CONTINUOUS_PICKUP = "continuous_pickup";
    public static final String CONTINUOUS_DROP_OFF = "continuous_drop_off";


    public static final String ID = ROUTE_ID;
    public static final String AGENCY = AGENCY_ID;
    public static final String SHORT_NAME = ROUTE_SHORT_NAME;
    public static final String LONG_NAME = ROUTE_LONG_NAME;
    public static final String DESC = ROUTE_DESC;
    public static final String TYPE = ROUTE_TYPE;
    public static final String URL = ROUTE_URL;
    public static final String COLOR = ROUTE_COLOR;
    public static final String TEXT_COLOR = ROUTE_TEXT_COLOR;
    public static final String SORT_ORDER = ROUTE_SORT_ORDER;
    public static final String PICKUP = CONTINUOUS_PICKUP;
    public static final String DROP_OFF = CONTINUOUS_DROP_OFF;

}
