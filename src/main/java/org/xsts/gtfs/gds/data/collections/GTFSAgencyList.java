/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gds.data.types.GTFSAgency;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GTFSAgencyList implements KeyValueList {
    private Map<String, GTFSAgency> agencies;

    public GTFSAgencyList(){
        agencies = new HashMap<>();
    }

    @Override
    public void add(String id, Object object) {
        add(id, (GTFSAgency) object);
    }
    public void add(String code, GTFSAgency GTFSAgency){
        agencies.put(code, GTFSAgency);
    }
    public GTFSAgency get(String code) {
        return agencies.get(code);
    }
    @Override
    public boolean isEmpty() { return agencies.size() == 0;}

    public GTFSAgency getFirstAgency() {
        return agencies.get(agencies.keySet().toArray()[0]);
    }
    public Set<String> keySet() { return agencies.keySet(); }
    public int count() { return agencies.size();}
}
