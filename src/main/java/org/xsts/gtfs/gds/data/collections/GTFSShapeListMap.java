/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gds.data.types.GTFSShape;

import java.util.HashMap;
import java.util.Map;

public class GTFSShapeListMap implements KeyValueList  {
    private Map<String, GTFSShapeList> shapesListMap;

    public GTFSShapeListMap() {
        shapesListMap = new HashMap<>();
    }

    GTFSShapeList getShapeList(String shapeID) {
        if (shapesListMap.containsKey(shapeID))
            return shapesListMap.get(shapeID);
        return null;
    }

    public Map<String, GTFSShapeList> getAll() {
        return shapesListMap;
    }

    public boolean isEmpty() { return shapesListMap.size() == 0;}

    public boolean contains(String shapeID) { return shapesListMap.containsKey(shapeID);}
    public void add(String shapeID, GTFSShapeList shapeList){
        shapesListMap.put(shapeID,shapeList);
    }


    @Override
    public void add(String s, Object o) {
        GTFSShape shape = (GTFSShape) o;
        if ( !contains(shape.id())) {
            GTFSShapeList shapeList = new GTFSShapeList();
            add(shape.id(), shapeList);
        }

        GTFSShapeList shapeList = get(shape.id());
        shapeList.add(shape);
    }

    public GTFSShapeList get(String shapeID) {
        return shapesListMap.get(shapeID);
    }
}
