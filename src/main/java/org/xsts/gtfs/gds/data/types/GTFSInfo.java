/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVRecord;

import java.util.Map;

public class GTFSInfo {
    public static final String SEMICOLON = ":";
    public static final String DQUOTE = "\"";
    public static final String COMMA = ",";
    public static final String SEMICOLON_QDUOTE = SEMICOLON + DQUOTE;
    public static final String QDUOTE_COMMA = DQUOTE + COMMA;

    public String getStringFieldOrNull(CSVRecord record, Map<String, Integer> positions, String fieldName) {
        String ret = null;

        if (positions.containsKey(fieldName))
            ret = record.get(fieldName).trim();
        return ret;
    }

    public Integer getIntegerFieldOrNull(CSVRecord record, Map<String, Integer> positions, String fieldName) {
        Integer ret = null;

        if (positions.containsKey(fieldName)) {
            ret = Integer.parseInt(record.get(fieldName));
        }

        return ret;
    }

    public Boolean getBooleanFieldOrNull(CSVRecord record, Map<String, Integer> positions, String fieldName) {
        Boolean ret = null;

        if (positions.containsKey(fieldName)) {

            ret = (Integer.parseInt(record.get(fieldName)) == 1);
        }

        return ret;
    }

    public String getIntFieldAsStringOrNull(CSVRecord record, Map<String, Integer> positions, String fieldName) {
        Integer intValue = null;
        String stringValue = null;

        if (positions.containsKey(fieldName)) {
            stringValue = record.get(fieldName);
            Integer tempValue = Integer.parseInt(stringValue);
            stringValue = tempValue.toString();
        }

        return stringValue;
    }

}
