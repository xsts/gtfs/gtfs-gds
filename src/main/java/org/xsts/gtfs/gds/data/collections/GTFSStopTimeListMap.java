/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.gtfs.gds.data.links.GTFSRouteTripLink;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.HashMap;
import java.util.Map;

public class GTFSStopTimeListMap {
    private Map<String, GTFSStopTimeList> stopTimesListMap;
    public GTFSStopTimeListMap(){
        stopTimesListMap = new HashMap<>();
    }

    public void add(String tripID, GTFSStopTimeList stopTimeList){
        stopTimesListMap.put(tripID,stopTimeList);
    }

    public GTFSStopTimeList get(String tripID) {
        return stopTimesListMap.get(tripID);
    }

    public Map<String, GTFSStopTimeList> getStopTimes() { return stopTimesListMap;}

    public boolean isEmpty() { return stopTimesListMap.size() == 0;};
    public boolean contains(String tripID) { return stopTimesListMap.containsKey(tripID);}


    public GTFSStopTimeList getLongestList() {
        GTFSStopTimeList stopTimeList = null;
        String key = null;
        for ( String s : stopTimesListMap.keySet()){
            if ( stopTimeList == null)
                stopTimeList = stopTimesListMap.get(s);
            else if ( stopTimesListMap.get(s).getStopTimes().size() > stopTimeList.getStopTimes().size()){
                stopTimeList = stopTimesListMap.get(s);
                key = s;
            }
        }
        return stopTimeList;
    }

    public static GTFSStopTimeList getLongestListFromGlobal(String routeID) {
        GTFSStopTimeList stopTimeList = null;
        Map<String, GTFSStopTime> longest = null;
        GTFSCache bigCache = GTFSCache.fromGlobal();

        GTFSRouteTripLink routeLink = bigCache.getLink().get(routeID);


        String key = null;
        for ( String key2 : routeLink.getLink().keySet()){
            if ( longest == null)
                longest = routeLink.getLink().get(key2).getLink();
            else if ( routeLink.getLink().get(key2).getLink().size() > longest.size()){
                longest = routeLink.getLink().get(key2).getLink();
                key = key2;
            }
        }

        stopTimeList =  GTFSStopTimeList.temporaryList(longest);

        return stopTimeList;
    }
}