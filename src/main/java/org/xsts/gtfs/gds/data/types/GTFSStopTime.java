/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSStopTimeFields;

import java.util.Map;

public class GTFSStopTime {
    private String trip;
    private String arrival;
    private String departure;
    private String stop;
    private String sequence;
    private String headsign;
    private String pickUp;
    private String dropOff;
    private String continuousPickUp;
    private String continuousDropOff;
    private String distance;
    private String timepoint;

    public GTFSStopTime(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        if (positions.containsKey(GTFSStopTimeFields.TRIP))
            trip = record.get(GTFSStopTimeFields.TRIP).trim();

        if (positions.containsKey(GTFSStopTimeFields.ARRIVAL))
            arrival = record.get(GTFSStopTimeFields.ARRIVAL).trim();

        if (positions.containsKey(GTFSStopTimeFields.DEPARTURE))
            departure = record.get(GTFSStopTimeFields.DEPARTURE).trim();

        if (positions.containsKey(GTFSStopTimeFields.STOP))
            stop = record.get(GTFSStopTimeFields.STOP).trim();

        if (positions.containsKey(GTFSStopTimeFields.SEQUENCE)) {
            sequence = record.get(GTFSStopTimeFields.SEQUENCE).trim();
            // Need to convert 1, 01, 001, 0001 into 1
            Integer tempValue = Integer.parseInt(sequence);
            sequence = tempValue.toString();
        }

        if (positions.containsKey(GTFSStopTimeFields.PICKUP))
            pickUp = record.get(GTFSStopTimeFields.PICKUP).trim();

        if (positions.containsKey(GTFSStopTimeFields.DROP_OFF))
            dropOff = record.get(GTFSStopTimeFields.DROP_OFF).trim();

        if (positions.containsKey(GTFSStopTimeFields.CONTINUOUS_PICKUP))
            continuousPickUp = record.get(GTFSStopTimeFields.CONTINUOUS_PICKUP).trim();

        if (positions.containsKey(GTFSStopTimeFields.CONTINUOUS_DROP_OFF))
            continuousDropOff = record.get(GTFSStopTimeFields.CONTINUOUS_DROP_OFF).trim();

        if (positions.containsKey(GTFSStopTimeFields.HEADSIGN))
            headsign = record.get(GTFSStopTimeFields.HEADSIGN).trim();

        if (positions.containsKey(GTFSStopTimeFields.DIST))
            distance = record.get(GTFSStopTimeFields.DIST).trim();

        if (positions.containsKey(GTFSStopTimeFields.TIMEPOINT))
            timepoint = record.get(GTFSStopTimeFields.TIMEPOINT).trim();


    }

    public String trip() { return trip;}
    public String arrival() { return arrival;}
    public String departure() { return departure;}
    public String stop() { return stop;}
    public String sequence() { return sequence;}
    public String headsign() { return headsign;}

    public String pickUp() { return pickUp;}
    public String dropOff() { return dropOff;}
    public String continuousPickUp() { return continuousPickUp;}
    public String continuousDropOff() { return continuousDropOff;}

    public String distance() { return distance;}
    public String timepoint() { return timepoint;}

}

