/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.collections;

import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gds.data.types.GTFSRoute;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GTFSRouteList implements KeyValueList {
    private Map<String, GTFSRoute> routes;

    public GTFSRouteList(){
        routes = new HashMap<>();
    }

    public void add(String id, GTFSRoute route){
        routes.put(id,route);
    }

    @Override
    public void add(String id, Object object) {
        add(id, (GTFSRoute) object);
    }

    public GTFSRoute get(String id) {
        return routes.get(id);
    }

    public GTFSRoute routeByShortName(String shortName) {
        for (String id: routes.keySet()){
            GTFSRoute route = routes.get(id);
            if (route.shortName() != null && route.shortName().compareTo(shortName) == 0){
                return route;
            }
        }
        return null;
    }

    public boolean isEmpty() { return routes.size() == 0;}

    public GTFSRoute getFirstRoute() {
        return routes.get(routes.keySet().toArray()[0]);
    }
    public Set<String> keySet() { return routes.keySet(); }
    public GTFSRouteList filter(Set<String> selectedRoutes) {
        GTFSRouteList result = new GTFSRouteList();
        for (String id: routes.keySet()){
            GTFSRoute localRoute = routes.get(id);
            if ( selectedRoutes.contains(localRoute.shortName())) {
                result.add(id, localRoute);
            }
        }

        return result;
    }

    public int count() { return routes.size();}

}