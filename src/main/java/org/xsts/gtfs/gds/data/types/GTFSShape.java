/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSShapeFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

public class GTFSShape extends GTFSInfo{
    private String uid;

    private String id;
    private String lat;
    private String lon;
    private String sequence;
    private String traveled;


    public GTFSShape(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();
        id          = getStringFieldOrNull(record, positions, GTFSShapeFields.ID );
        lat        = getStringFieldOrNull(record, positions,GTFSShapeFields.LAT );
        lon         = getStringFieldOrNull(record, positions,GTFSShapeFields.LON );
        sequence    = getStringFieldOrNull(record, positions,GTFSShapeFields.SEQUENCE );
        traveled        = getStringFieldOrNull(record, positions,GTFSShapeFields.TRAVELED );

    }

    public String id() { return id;}
    public String lat() { return lat;}
    public String lon() { return lon;}
    public String sequence() { return sequence;}
    public String traveled() { return traveled;}


    public String toString() {
        StringBuilder sb =  new StringBuilder();
        if ( id != null && id.length() > 0){
            sb.append("ID: " ).append(id).append("   ");
        }

        if ( lat != null && lat.length() > 0){
            sb.append("Latitude: " ).append(lat).append("   ");
        }

        if ( lon != null && lon.length() > 0){
            sb.append("Longitude: " ).append(lon).append("   ");
        }

        if ( sequence != null && sequence.length() > 0){
            sb.append("Sequence: " ).append(sequence).append("   ");
        }

        if ( traveled != null && traveled.length() > 0){
            sb.append("Dist traveled: " ).append(traveled).append("   ");
        }


        return sb.toString();
    }

    public String uid() {return uid; }
    public void register() {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        uid = bigCache.registerAgency();
    }
}

