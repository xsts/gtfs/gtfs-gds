/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSPathwayMode {
    public static final Integer WALKWAY = 1;
    public static final Integer STAIRS = 2;
    public static final Integer MOVING_SIDEWALK = 3;
    public static final Integer ESCALATOR = 4;
    public static final Integer ELEVATOR = 5;
    public static final Integer FARE_GATE = 6;
    public static final Integer EXIT_GATE = 7;

    public static final Integer TRAVELATOR = MOVING_SIDEWALK;
}

