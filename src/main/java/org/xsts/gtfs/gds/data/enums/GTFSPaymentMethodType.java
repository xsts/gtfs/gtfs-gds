/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSPaymentMethodType {
    public static final Integer AFTER_BOARDING = 0;
    public static final Integer BEFORE_BOARDING = 1;

    public static final Integer ON_BOARD = AFTER_BOARDING;

}
