/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSTranslationFields {
    public static final String  TABLE_NAME = "table_name";
    public static final String  FIELD_NAME = "field_name";
    public static final String  LANGUAGE = "language";
    public static final String  TRANSLATION = "translation";
    public static final String  RECORD_ID = "record_id";
    public static final String  RECORD_SUB_ID = "record_sub_id";
    public static final String  FIELD_VALUE = "field_value";

    public static final String TABLE = TABLE_NAME;
    public static final String FIELD = FIELD_NAME;
    public static final String LANG = LANGUAGE;
    public static final String TRN_VALUE = TRANSLATION;
    public static final String RECORD = RECORD_ID;
    public static final String RECORD_SUB = RECORD_SUB_ID;
    public static final String FLD_VALUE = FIELD_VALUE;

}
