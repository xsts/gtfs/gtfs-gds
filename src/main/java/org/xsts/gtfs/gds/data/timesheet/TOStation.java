/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.timesheet;

public class TOStation {
    private String name;
    private Integer  timeOffset;
    private Long  latitude;
    private Long  longitude;
    private Integer order;

    public TOStation(){
        this.name = "<name>";
        this.timeOffset = 0;
        this.latitude = 0L;
        this.longitude = 0L;
        this.order = 0; // unused
    }

    public TOStation(String name, Integer  timeOffset) {
        this.name = name;
        this.timeOffset = timeOffset;
        this.latitude = 0L;
        this.longitude = 0L;
        this.order = 0;
    }

    public TOStation(String name, Integer  timeOffset, Long latitude, Long longitude) {
        this.name = name;
        this.timeOffset = timeOffset;
        this.latitude = latitude;
        this.longitude = longitude;
        this.order = 0;
    }

    public TOStation(String name, Integer  timeOffset, Long latitude, Long longitude, Integer order) {
        this.name = name;
        this.timeOffset = timeOffset;
        this.latitude = latitude;
        this.longitude = longitude;
        this.order = order;
    }


    public double degLatitude() { return ((double) latitude) / 1000000.0;}
    public double degLongitude() { return ((double) longitude) / 1000000.0;}

    public String name() { return this.name;}
    public Integer timeOffset()  { return this.timeOffset;}
    public Long latitude() { return this.latitude;}
    public Long longitude() { return this.longitude; }
    public Integer order()  { return this.order;}


    public TOStation name(String name) {this.name = name; return this;}
    public TOStation timeOffset(Integer  timeOffset) {this.timeOffset = timeOffset;return this;}
    public TOStation latitude(Long latitude) { this.latitude = latitude;return this;}
    public TOStation longitude(Long longitude) { this.longitude = longitude;return this;}
    public TOStation order(Integer  order) {this.order = order;return this;}


    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{ ");
        builder.append("name: ").append(name);
        builder.append(", ").append("time_offset: ").append(timeOffset);
        builder.append(", ").append("latitude: ").append(latitude);
        builder.append(", ").append("longitude: ").append(longitude);
        builder.append(" }");
        return builder.toString();
    }
}
