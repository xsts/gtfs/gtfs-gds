/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

public class GTFSUniqueCodeIdentifier {
    public static final String AGENCY_SIGNATURE = "A#";
    public static final String ROUTE_SIGNATURE = "R#";
    public static final String STOP_SIGNATURE = "T#";
    public static final String TRIP_SIGNATURE = "S#";
    public static final String ERROR_SIGNATURE = "E#";


    public static final int ROUTE_TYPE = 1;
    public static final int STOP_TYPE = 2;
    public static final int TRIP_TYPE = 3;
    public static final int TRANSFER_TYPE = 4;
    public static final int AGENCY_TYPE = 5;


    public static final String SEPARATOR = "@";

    private String uid = null;

    private static int seed  = 0;

    public static GTFSUniqueCodeIdentifier generate(int type){
        seed++;
        return new GTFSUniqueCodeIdentifier(type, seed);
    }

    protected GTFSUniqueCodeIdentifier(int type, int number){
        StringBuilder sb = new StringBuilder();
        switch(type){
            case ROUTE_TYPE:
                sb.append(ROUTE_SIGNATURE);
                break;
            case STOP_TYPE:
                sb.append(STOP_SIGNATURE);
                break;
            case TRIP_TYPE:
                sb.append(TRIP_SIGNATURE);
                break;
            case AGENCY_TYPE:
                sb.append(AGENCY_SIGNATURE);
                break;
            default:
                sb.append(ERROR_SIGNATURE);
                break;
        }

        sb.append(SEPARATOR).append(number);
        this.uid = sb.toString();
        sb = null;
    }

    public String uid() { return uid; }
}
