/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.enums;

public class GTFSExactTimeType {
    public static final Integer FREQUENCY_BASED = 0;
    public static final Integer FREQUENCY_BASED_DEFAULT = null;

    public static final Integer SCHEDULE_BASED = 1;
}
