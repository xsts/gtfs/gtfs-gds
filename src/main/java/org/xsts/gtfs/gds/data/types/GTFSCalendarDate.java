/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.types;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gds.data.fields.GTFSCalendarDateFields;

import java.util.Map;

public class GTFSCalendarDate {
    private String uid;
    private String service;
    private String date;
    private String exception;


    public GTFSCalendarDate(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();

        if (positions.containsKey(GTFSCalendarDateFields.SERVICE))
            service = record.get(positions.get(GTFSCalendarDateFields.SERVICE)).trim();

        if (positions.containsKey(GTFSCalendarDateFields.DATE))
            date = record.get(GTFSCalendarDateFields.DATE).trim();

        if (positions.containsKey(GTFSCalendarDateFields.EXCEPTION))
            exception = record.get(GTFSCalendarDateFields.EXCEPTION).trim();
    }

    public String service() { return service;}
    public String date() { return date;}
    public String exception() { return exception;}

}
