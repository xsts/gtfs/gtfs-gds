/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSTransferFields {
    public static final String FROM_STOP_ID = "from_stop_id";
    public static final String TO_STOP_ID = "to_stop_id";
    public static final String TRANSFER_TYPE = "transfer_type";
    public static final String MIN_TRANSFER_TIME = "min_transfer_time";


    public static final String FROM_STOP = FROM_STOP_ID;
    public static final String TO_STOP = TO_STOP_ID;
    public static final String TYPE = TRANSFER_TYPE;
    public static final String MIN_TIME = MIN_TRANSFER_TIME;

    public static final String FROM = FROM_STOP;
    public static final String TO = TO_STOP;
    public static final String TRANSFER_TIME = MIN_TIME;

    public static final String TIME = TRANSFER_TIME;
}

