/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Structures
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.gtfs.gds.data.fields;

public class GTFSCalendarDateFields {
    public static final String SERVICE_ID = "service_id";
    public static final String DATE = "date";
    public static final String EXCEPTION_TYPE = "exception_type";

    public static final String SERVICE = SERVICE_ID;
    public static final String EXCEPTION = EXCEPTION_TYPE;

}
